Require Import String Ascii Orders Omega List Sorted Permutation.
Require Import Coq.Program.Equality.

(* eqb *)

Definition eqb_ascii (a b : ascii) : bool :=
 match a, b with
 | Ascii a0 a1 a2 a3 a4 a5 a6 a7,
   Ascii b0 b1 b2 b3 b4 b5 b6 b7 =>
    Bool.eqb a0 b0 && Bool.eqb a1 b1 && Bool.eqb a2 b2 && Bool.eqb a3 b3
    && Bool.eqb a4 b4 && Bool.eqb a5 b5 && Bool.eqb a6 b6 && Bool.eqb a7 b7
 end.

Fixpoint eqb_string (s1 s2 : string) : bool :=
  match s1, s2 with
  | EmptyString, EmptyString => true
  | String a1 s1, String a2 s2 => andb (eqb_ascii a1 a2) (eqb_string s1 s2)
  | _, _ => false
  end.

(* lt *)

Fixpoint lexico_lt (s1 s2: string) : Prop :=
  match (s1, s2) with
  | (EmptyString, EmptyString) => False
  | (EmptyString, String _ _) => True
  | (String _ _, EmptyString) => False
  | (String a1 s1, String a2 s2) =>
    let n1 := (nat_of_ascii a1) in
    let n2 := (nat_of_ascii a2) in
    if (Nat.ltb n1 n2) then True else
      if (Nat.ltb n2 n1) then False else lexico_lt s1 s2
  end.

Ltac destruct_two a0 a1 eq :=
  destruct (Nat.ltb (nat_of_ascii a0) (nat_of_ascii a1)) eqn:eq.

Ltac assert_two a0 a1 eq :=
  try assert (Nat.ltb (nat_of_ascii a0) (nat_of_ascii a1) = true) as eq by (rewrite Nat.ltb_lt in *; rewrite Nat.ltb_nlt in *; omega);
  try assert (Nat.ltb (nat_of_ascii a0) (nat_of_ascii a1) = false) as eq by (rewrite Nat.ltb_lt in *; rewrite Nat.ltb_nlt in *; omega).

Ltac my_omega :=
  try rewrite Nat.ltb_lt in *; try rewrite Nat.ltb_nlt in *; omega.

Theorem lexico_trans : forall s1 s2 s3,
    lexico_lt s1 s2 -> lexico_lt s2 s3 -> lexico_lt s1 s3.
Proof.
  induction s1; intros s2 s3 H12 H23.
  - destruct s2; destruct s3; auto.
  - destruct s2; destruct s3; auto; simpl in *.
    inversion H12.
    destruct_two a a1 aa1. exact I.
    destruct_two a1 a a1a; destruct_two a a0 aa0.
    assert_two a0 a1 a0a1; rewrite a0a1 in *.
    assert_two a1 a0 a1a0; rewrite a1a0 in *. inversion H23.
    destruct_two a0 a a0a. inversion H12.
    assert_two a0 a1 a0a1; rewrite a0a1 in *.
    assert_two a1 a0 a1a0; rewrite a1a0 in *. inversion H23.
    assert_two a0 a1 a0a1; rewrite a0a1 in *. assert_two a1 a0 a1a0; rewrite a1a0 in *.
    inversion H23.
    destruct_two a0 a a0a. inversion H12.
    destruct_two a0 a1 a0a1. my_omega.
    destruct_two a1 a0 a1a0. my_omega.
    eapply IHs1; eauto.
Qed.

Theorem lexico_antisym : forall s1 s2,
    lexico_lt s1 s2 -> lexico_lt s2 s1 -> s1 = s2.
Proof.
  induction s1; intros s2 H12 H21.
  - destruct s2; simpl in *; auto. inversion H21.
  - destruct s2; simpl in *; auto. inversion H12.
    destruct_two a a0 aa0; destruct_two a0 a a0a; try my_omega.
    specialize (IHs1 s2 H12 H21); subst.
    assert (nat_of_ascii a = nat_of_ascii a0) by my_omega.
    assert (ascii_of_nat (nat_of_ascii a) = ascii_of_nat (nat_of_ascii a0)) by auto.
    repeat rewrite ascii_nat_embedding in H0. rewrite H0; reflexivity.
Qed.

Theorem lexico_irrefl : forall s,
    ~(lexico_lt s s).
Proof.
  intros s contra.
  induction s; simpl in contra; auto.
  rewrite Nat.ltb_irrefl in contra; auto.
Qed.

Corollary lexico_antirefl : forall s1 s2,
    lexico_lt s1 s2 -> ~(lexico_lt s2 s1).
Proof.
  intros s1 s2 Hlt contra.
  assert (s1 = s2) by (apply lexico_antisym; auto); subst.
  specialize (lexico_irrefl s2) as contra2. contradiction.
Qed.

Lemma Sorted_not_In {t} : forall s v v' ls,
    Sorted.StronglySorted lexico_lt (map (fun pat_ : string * t => let (l, _) := pat_ in l) ((s, v)::ls)) ->
    ~ In (s, v') ls.
Proof.
  intros s v v' ls HSort.
  induction ls; simpl.
  - intro contra; inversion contra.
  - intros contra. destruct a as [s' v'']. destruct contra; inversion HSort; subst.
    + inversion H; subst; clear H. inversion H3. apply lexico_irrefl in H1; auto.
    + apply IHls; auto. inversion H2; inversion H3; subst. constructor; auto.
Qed.


Inductive Join {t} : list (string * t) -> list (string * t) -> list (string * t) -> Prop :=
| JoinVNil v : Join v nil v
| JoinNilV v : Join nil v v
| JoinLeft l1 l2 t1 t2 v1 v2 v3 :
    Join v1 ((l2, t2)::v2) v3 ->
    lexico_lt l1 l2 ->
    Join ((l1, t1)::v1) ((l2, t2)::v2) ((l1, t1)::v3)
| JoinRight l1 l2 t1 t2 v1 v2 v3 :
    Join ((l1, t1)::v1) v2 v3 ->
    lexico_lt l2 l1 ->
    Join ((l1, t1)::v1) ((l2, t2)::v2) ((l2, t2)::v3).

Lemma Join_Forall_le {t} : forall l ls1 ls2 ls3,
    Forall (lexico_lt l) (map (fun p : (string * t) => let (l, _) := p in l) ls1) ->
    Forall (lexico_lt l) (map (fun p : (string * t) => let (l, _) := p in l) ls2) ->
    Join ls1 ls2 ls3 ->
    Forall (lexico_lt l) (map (fun p : (string * t) => let (l, _) := p in l) ls3).
Proof.
  intros l ls1 ls2 ls3 HF1 HF2 HJoin.
  induction HJoin; auto; inversion HF1; inversion HF2; subst.
  - constructor; auto.
  - constructor; auto.
Qed.

Lemma Forall_lexico_trans {t} : forall ls l1 l2,
    lexico_lt l1 l2 ->
    Forall (lexico_lt l2) (map (fun p : (string * t) => let (l, _) := p in l) ls) ->
    Forall (lexico_lt l1) (map (fun p : (string * t) => let (l, _) := p in l) ls).
Proof.
  intros ls l1 l2 Hle HF.
  induction ls; simpl. auto.
  destruct a. inversion HF; subst.
  constructor; auto. eapply lexico_trans; eauto.
Qed.

Theorem Join_IsSorted {t} : forall ls1 ls2 ls3,
    StronglySorted lexico_lt (map (fun p : (string * t) => let (l, _) := p in l) ls1) ->
    StronglySorted lexico_lt (map (fun p : (string * t) => let (l, _) := p in l) ls2) ->
    Join ls1 ls2 ls3 ->
    StronglySorted lexico_lt (map (fun p : (string * t) => let (l, _) := p in l) ls3).
Proof.
  intros ls1 ls2 ls3 HS1 HS2 HJoin.
  induction HJoin; auto.
  - inversion HS1; subst. specialize (IHHJoin H2 HS2).
    constructor; auto.
    specialize (@Join_Forall_le t l1 v1 ((l2, t2)::v2) v3) as HForall.
    apply HForall; clear HForall; auto.
    constructor; auto.
    inversion HS2; subst. eapply (@Forall_lexico_trans t); eauto.
  - inversion HS2; subst. specialize (IHHJoin HS1 H2).
    constructor; auto.
    specialize (@Join_Forall_le t l2 ((l1, t1)::v1) v2 v3) as HForall.
    apply HForall; clear HForall; auto.
    constructor; auto.
    inversion HS1; subst. eapply (@Forall_lexico_trans t); eauto.
Qed.

Theorem Join_Permutation {t} : forall ls1 ls2 ls3,
    @Join t ls1 ls2 ls3 ->
    Permutation (ls1++ls2) ls3.
Proof.
  intros ls1 ls2 ls3 HJoin.
  induction HJoin; simpl in *; auto.
  - rewrite app_nil_r; auto.
  - eapply Permutation_trans.
    2: eapply perm_skip; apply IHHJoin.
    eapply Permutation_sym. eapply Permutation_trans. eapply perm_swap.
    apply perm_skip. apply Permutation_middle.
Qed.

Lemma Join_nil {t} : forall ls1 ls2,
    @Join t ls1 ls2 nil ->
    (ls1 = nil /\ ls2 = nil).
Proof.
  intros ls1 ls2 HJoin.
  inversion HJoin; subst; auto.
Qed.

Lemma Join_In_l {t} : forall v ls1 ls2 ls3,
    @Join t ls1 ls2 ls3 ->
    In v ls1 -> In v ls3.
Proof.
  intros v ls1 ls2 ls3 HJoin HIn.
  induction HJoin; auto.
  - inversion HIn.
  - inversion HIn; subst.
    + constructor; reflexivity.
    + apply in_cons; auto.
  - inversion HIn; subst; apply in_cons; auto.
Qed.

Corollary Join_In_l_cons {t} : forall v ls1 ls2 ls3,
    @Join t (v::ls1) ls2 ls3 ->
    In v ls3.
Proof.
  intros v ls1 ls2 ls3 HJoin.
  eapply Join_In_l; eauto. constructor; reflexivity.
Qed.

Lemma Join_In_r {t} : forall v ls1 ls2 ls3,
    @Join t ls1 ls2 ls3 ->
    In v ls2 -> In v ls3.
Proof.
  intros v ls1 ls2 ls3 HJoin HIn.
  induction HJoin; auto.
  - inversion HIn.
  - inversion HIn; subst; apply in_cons; auto.
  - inversion HIn; subst.
    + constructor; reflexivity.
    + apply in_cons; auto.
Qed.

Lemma Join_In_inv {t} : forall v ls3,
    In v ls3 ->
    StronglySorted lexico_lt (map (fun p : (string * t) => let (l, _) := p in l) ls3) ->
    exists ls2, @Join t (v::nil) ls2 ls3.
Proof.
  intros v ls3 HIn HSort.
  induction ls3.
  - inversion HIn.
  - inversion HIn; subst.
    + exists ls3. destruct ls3; try constructor.
      destruct v as [s1 v1]; destruct p as [s2 v2].
      constructor; try constructor. simpl in *.
      inversion HSort; subst. inversion H2; auto.
    + inversion HSort; subst. specialize (IHls3 H H2).
      destruct IHls3 as [ls2 IHls3].
      destruct v as [s1 v1]; destruct a as [s2 v2].
      exists ((s2, v2)::ls2). constructor; auto.
      rewrite Forall_forall in H3. apply H3.
      change s1 with ((fun p : string * t => let (l, _) := p in l) (s1, v1)).
      apply in_map; assumption.
Qed.

(* le, used for Mergesort *)

Fixpoint lexico_le (s1 s2: string) : Prop :=
  match (s1, s2) with
  | (EmptyString, _) => True
  | (String _ _, EmptyString) => False
  | (String a1 s1, String a2 s2) =>
    let n1 := (nat_of_ascii a1) in
    let n2 := (nat_of_ascii a2) in
    if (Nat.ltb n1 n2) then True else
      if (Nat.ltb n2 n1) then False else lexico_le s1 s2
  end.

Lemma lexico_lt_le : forall s1 s2,
    lexico_lt s1 s2 -> lexico_le s1 s2.
Proof.
  induction s1; intros s2 Hlt; destruct s2; simpl in *; try inversion Hlt.
  - exact I.
  - destruct_two a a0 aa0. exact I.
    destruct_two a0 a a0a. destruct_two a a0 aa0lt; inversion Hlt.
    eapply IHs1; eauto.
Qed.

Theorem lexico_total : forall s1 s2,
    lexico_le s1 s2 \/ lexico_le s2 s1.
Proof.
  induction s1; intro s2.
  - destruct s2; simpl; auto.
  - destruct s2; simpl; auto.
    destruct_two a a0 aa0; destruct_two a0 a a0a; auto.
Qed.

Theorem lexico_le_dec : forall a,
    (forall x, {lexico_le a x} + {~ lexico_le a x}).
Proof.
  induction a; intros x; destruct x; simpl; auto.
  - destruct_two a a1 aa1; auto.
    destruct_two a1 a a1a; auto.
Qed.

Corollary lexico_le_or : forall a,
    (forall x, lexico_le a x \/ ~ lexico_le a x).
Proof.
  intros a x.
  specialize (lexico_le_dec a x) as [dec|dec]; auto.
Qed.

Fixpoint lexico_leb (s1 s2: string) : bool :=
  match (s1, s2) with
  | (EmptyString, _) => true
  | (String _ _, EmptyString) => false
  | (String a1 s1, String a2 s2) =>
    let n1 := (nat_of_ascii a1) in
    let n2 := (nat_of_ascii a2) in
    if (Nat.ltb n1 n2) then true else
      if (Nat.ltb n2 n1) then false else lexico_leb s1 s2
  end.

Theorem lexico_leb_le : forall s1 s2,
    lexico_leb s1 s2 = true <-> lexico_le s1 s2.
Proof.
  induction s1; intros s2; destruct s2; simpl in *; split; intros H12;
    try (destruct_two a a0 aa0); try (destruct_two a0 a a0a);
    try exact I; try reflexivity; try discriminate H12; try inversion H12;
      try (apply IHs1; auto).
Qed.

Definition lexico_leb_pair {t} (p1 p2: (string * t)) : bool :=
  let (s1, _) := p1 in let (s2, _) := p2 in lexico_leb s1 s2.

Theorem lexico_leb_pair_le : forall t s1 s2 (t1 t2:t),
    lexico_leb_pair (s1, t1) (s2, t2) = true <-> lexico_le s1 s2.
Proof.
  induction s1; intros s2; destruct s2; simpl in *; split; intros H12;
    try (destruct_two a a0 aa0); try (destruct_two a0 a a0a);
    try exact I; try reflexivity; try discriminate H12; try inversion H12;
      try (apply IHs1; auto).
Qed.

Definition lexico_leb_triple {t1 t2} (p1 p2: (string * t1 * t2)) : bool :=
  let '(s1, _, _) := p1 in let '(s2, _, _) := p2 in lexico_leb s1 s2.

Theorem lexico_triple_leb_le : forall t t' s1 s2 (v1 v2:t) (v1' v2':t'),
    lexico_leb_triple (s1, v1, v1') (s2, v2, v2') = true <-> lexico_le s1 s2.
Proof.
  induction s1; intros s2; destruct s2; simpl in *; split; intros H12;
    try (destruct_two a a0 aa0); try (destruct_two a0 a a0a);
    try exact I; try reflexivity; try discriminate H12; try inversion H12;
      try (apply IHs1; auto).
Qed.

(* Module Type TType. *)
(*   Parameter T : Type. *)
(* End TType. *)

(* Module LexicoOrder (T: TType) <: TotalLeBool. *)
(*   Definition t := (string * T.T)%type. *)
(*   Definition leb := @lexico_leb_pair T.T. *)
(*   Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true. *)
(*   Proof. *)
(*     intros p1 p2. destruct p1 as [s1 t1]; destruct p2 as [s2 t2]. *)
(*     unfold leb. repeat rewrite lexico_leb_le. *)
(*     apply lexico_total. *)
(*   Qed. *)
(* End LexicoOrder. *)

(* Inductive NotSorted : list string -> Prop := *)
(*   | NotSortedCons1 a l : NotSorted l -> NotSorted (a::l) *)
(*   | NotSortedCons2 a l : Exists (fun s => ~(lexico_le a s)) l -> NotSorted (a::l). *)

(* Theorem StronglySorted_NotSorted_dec : forall l, *)
(*      { StronglySorted lexico_le l } + { NotSorted l }. *)
(* Proof. *)
(*   induction l. *)
(*   - left. constructor. *)
(*   - destruct IHl. *)
(*     + specialize (Forall_Exists_dec (lexico_le a) (lexico_le_dec a) l). *)
(*       intros [Hforall|Hexists]. *)
(*       * left. constructor; auto. *)
(*       * right. apply NotSortedCons2; auto. *)
(*     + right. constructor; auto. *)
(* Qed. *)

(* Corollary StronglySorted_NotSorted_iff: forall l, *)
(*     NotSorted l <-> ~(StronglySorted lexico_le l). *)
(* Proof. *)
(*   induction l. *)
(*   - split; intros. inversion H. exfalso. apply H; constructor. *)
(*   - split; intros. *)
(*     + intro contra. inversion H; subst. rewrite IHl in H1. *)
(*       apply H1. inversion contra; auto. *)
(*       inversion contra; subst. *)
(*       specialize (Exists_Forall_neg (lexico_le a) l (lexico_le_or a)) as Hneg. *)
(*       rewrite Hneg in H1; apply H1; auto. *)
(*     + specialize (StronglySorted_NotSorted_dec (a::l)) as [HStrong|HNot]; auto. *)
(*       exfalso. auto. *)
(* Qed. *)
