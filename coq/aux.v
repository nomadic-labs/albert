(* Auxiliary lemmas *)

Require Import albert.
Require Import List Permutation.
Require Import Ott.ott_list_distinct.

Lemma ott_list_mem_perm : forall (A:Set) eq_dec x (l1 l2: list A),
    Permutation l1 l2 ->
    ott_list_core.list_mem eq_dec x l1 = ott_list_core.list_mem eq_dec x l2.
Proof.
  intros A eq_dec x l1 l2 HPerm.
  destruct (ott_list_core.list_mem eq_dec x l1) eqn:Hmem; symmetry.
  - apply Bool.Is_true_eq_left in Hmem.
    apply ott_list_mem.list_mem_implies_In in Hmem.
    eapply Permutation_in in HPerm; try eassumption.
    eapply ott_list_mem.In_implies_list_mem in HPerm.
    eapply Bool.Is_true_eq_true. eassumption.
  - rewrite <- Bool.not_true_iff_false. rewrite <- Bool.not_true_iff_false in Hmem.
    unfold not in *. intro contra. apply Hmem. clear Hmem.
    apply Bool.Is_true_eq_left in contra.
    apply ott_list_mem.list_mem_implies_In in contra.
    eapply Permutation_in in contra; try (eapply Permutation_sym; eassumption).
    eapply ott_list_mem.In_implies_list_mem in contra.
    eapply Bool.Is_true_eq_true. eassumption.
Qed.

Lemma ott_distinct_perm : forall (A:Set) eq_dec (l1 l2: list A),
    ott_list_distinct.all_distinct eq_dec l1 = true ->
    Permutation l1 l2 ->
    ott_list_distinct.all_distinct eq_dec l2 = true.
Proof.
  intros A eq_dec l1 l2 Hdistinct HPerm.
  induction HPerm.
  - (* nil *) auto.
  - (* x::l' *)
    simpl in *. apply andb_prop in Hdistinct. destruct Hdistinct as [Hmem Hl].
    specialize (IHHPerm Hl). rewrite IHHPerm.
    apply andb_true_intro. split; try reflexivity.
    erewrite ott_list_mem_perm. eassumption. apply Permutation_sym; assumption.
  - (* x::y::l' *)
    simpl in *. destruct (eq_dec x y) eqn:Hlab; simpl in Hdistinct;
    destruct (eq_dec y x) eqn:Hlab2; try discriminate Hdistinct.
    subst. rewrite Hlab in Hlab2. discriminate Hlab2.
    apply andb_prop in Hdistinct. destruct Hdistinct as [Hd1 Hd2].
    apply andb_prop in Hd2. destruct Hd2 as [Hd2 Hd3].
    repeat (apply andb_true_intro; split); try assumption.
  - auto.
Qed.

(* Some lemmas on tuples destructuring and restructuring *)

Lemma map_tuple_1 : forall T1 T2 T3 l,
    map (fun x : T1 * T2 * T3 => let (l__, _) := let (p, ty__) := x in let (_, x__) := p in (x__, ty__) in l__) l =
    map (fun pat_ : T1 * T2 * T3 => let (p, _) := pat_ in let (_, x__) := p in x__) l.
Proof.
  induction l.
  - reflexivity.
  - simpl. rewrite IHl. destruct a as [[l1 l2] ty]. reflexivity.
Qed.

Lemma map_tuple_2 : forall T1 T2 T3 l,
    map (fun x : T1 * T2 * T3 => let (_, ty__) := let (p, ty__) := x in let (_, x__) := p in (x__, ty__) in ty__) l =
    map (fun x : T1 * T2 * T3 => let (_, ty__0) := let (p, ty__0) := x in let (l__, _) := p in (l__, ty__0) in ty__0) l.
Proof.
  induction l.
  - reflexivity.
  - simpl. rewrite IHl. destruct a as [[l1 l2] ty]. reflexivity.
Qed.

Lemma map_tuple_3 : forall T1 T2 T3 l,
    (map (fun x : T1 * T2 * T3 => let (l__, _) := let (p, ty__) := x in let (l__, _) := p in (l__, ty__) in l__) l) =
    (map (fun pat_ : T1 * T2 * T3  => let (p, _) := pat_ in let (l__, _) := p in l__) l).
Proof.
  induction l.
  - reflexivity.
  - simpl. rewrite IHl. destruct a as [[l1 l2] ty]. reflexivity.
Qed.

Lemma map_tuple_4 : forall T1 T2 T3 l,
    map (fun x : T1 * T2 * T3 => let (l__, _) := let (p, ty'__) := x in let (l__, _) := p in (l__, ty'__) in l__) l =
    map (fun x : T1 * T2 * T3 => let (l__, _) := let (p, _) := x in let (l__, ty__) := p in (l__, ty__) in l__) l.
Proof.
  induction l.
  - reflexivity.
  - simpl. rewrite IHl. destruct a as [[l1 l2] ty]. reflexivity.
Qed.

Lemma map_tuple_5 : forall T1 T2 T3 l ty,
    In ty (map (fun x : T1 * T2 * T3 => let (_, ty__) := let (p, ty__) := x in let (l__, _) := p in (l__, ty__) in ty__) l) ->
    exists val, In (val, ty) (map (fun pat_ : T1 * T2 * T3 => let (p, ty__0) := pat_ in let (_, val__) := p in (val__, ty__0)) l).
Proof.
  induction l; intros ty HIn.
  - simpl in HIn. inversion HIn.
  - simpl in *. destruct a as [[x y] z].
    destruct HIn.
    + subst. exists y. left. f_equal; auto.
    + specialize (IHl ty H).
      destruct IHl as [val IHl]. exists val; auto.
Qed.

Lemma map_tuple_6 : forall T1 T2 T3 l ty',
    In ty' (map (fun x : T1 * T2 * T3 => let (_, ty__) := let (p, ty'__) := x in let (l__, _) := p in (l__, ty'__) in ty__) l) ->
    exists ty, In (ty, ty') (map (fun pat_ : T1 * T2 * T3 => let (p, ty'__0) := pat_ in let (_, ty__0) := p in (ty__0, ty'__0)) l).
Proof.
  induction l; intros ty' HIn.
  - simpl in HIn. inversion HIn.
  - simpl in *. destruct a as [[x y] z].
    destruct HIn.
    + subst. exists y. left. f_equal; auto.
    + specialize (IHl ty' H).
      destruct IHl as [ty IHl]. exists ty; auto.
Qed.

Lemma map_tuple_6' : forall T1 T2 T3 l ty ty',
    In (ty, ty') (map (fun pat_ : T1 * T2 * T3 => let (p, ty'__0) := pat_ in let (_, ty__0) := p in (ty__0, ty'__0)) l) ->
    In ty (map (fun x : T1 * T2 * T3 => let (_, ty__0) := let (p, _) := x in let (l__, ty__0) := p in (l__, ty__0) in ty__0) l).
Proof.
  induction l; intros ty ty' HIn.
  - simpl in HIn. inversion HIn.
  - simpl in *. destruct a as [[x y] z].
    destruct HIn.
    + subst. inversion H. left; auto.
    + right. eapply IHl; eassumption.
Qed.

Lemma map_tuple_7 : forall T1 T2 T3 l ty,
    In ty (map (fun x : T1 * T2 * T3 => let (_, ty__) := let (p, _) := x in let (l__, ty__) := p in (l__, ty__) in ty__) l) ->
    exists ty', In (ty, ty') (map (fun pat_ : T1 * T2 * T3 => let (p, ty'__0) := pat_ in let (_, ty__0) := p in (ty__0, ty'__0)) l).
Proof.
  induction l; intros ty HIn.
  - simpl in HIn. inversion HIn.
  - simpl in *. destruct a as [[x y] z].
    destruct HIn.
    + subst. exists z. left. f_equal; auto.
    + specialize (IHl ty H).
      destruct IHl as [ty' IHl]. exists ty'; auto.
Qed.

Lemma map_tuple_7' : forall T1 T2 T3 l ty ty',
    In (ty, ty') (map (fun pat_ : T1 * T2 * T3 => let (p, ty'__0) := pat_ in let (_, ty__0) := p in (ty__0, ty'__0)) l) ->
    In ty' (map (fun x : T1 * T2 * T3 => let (_, ty__) := let (p, ty'__) := x in let (l__, _) := p in (l__, ty'__) in ty__) l).
Proof.
  induction l; intros ty ty' HIn.
  - simpl in HIn. inversion HIn.
  - simpl in *. destruct a as [[x y] z].
    destruct HIn.
    + subst. inversion H. left; auto.
    + right. eapply IHl; eassumption.
Qed.

Lemma combine_nil_nil : forall A B (l : list A) (l' : list B),
    length l = length l' ->
    combine l l' = nil ->
    (l = nil /\ l' = nil).
Proof.
  intros A B l l' Hlen Hcomb.
  destruct l; simpl in *.
  - symmetry in Hlen. rewrite length_zero_iff_nil in Hlen; subst; auto.
  - destruct l'; simpl in *. discriminate Hlen. discriminate Hcomb.
Qed.

Lemma combine_inv_l : forall A B (l : list A) (l' : list B),
    length l = length l' ->
    map (fun pat_ : A * B => let (a, _) := pat_ in a) (combine l l') = l.
Proof.
  intros A B l.
  induction l; intros l' Hlen; simpl in *.
  - reflexivity.
  - destruct l'. inversion Hlen.
    simpl in Hlen. inversion Hlen; clear Hlen.
    specialize (IHl l' H0). simpl. rewrite IHl. reflexivity.
Qed.

Lemma combine_inv_r : forall A B (l : list A) (l' : list B),
    length l = length l' ->
    map (fun pat_ : A * B => let (_, b) := pat_ in b) (combine l l') = l'.
Proof.
  intros A B l l'.
  generalize dependent l.
  induction l'; intros l Hlen; simpl in *.
  - destruct l; reflexivity.
  - destruct l. inversion Hlen.
    simpl in Hlen. inversion Hlen; clear Hlen.
    specialize (IHl' l H0). simpl. rewrite IHl'. reflexivity.
Qed.

(** Some cool list induction principles *)

Section list_pair_ind.
  Variable A : Type.
  Variable B : Type.
  Variable P : list A -> list B -> Prop.

  Hypothesis Hnil : P nil nil.
  Hypothesis Hcons : forall tl1 tl2 a1 a2,
      length tl1 = length tl2 ->
      P tl1 tl2 -> P (a1::tl1) (a2::tl2).

  Theorem list_pair_ind : forall l1 l2,
      length l1 = length l2 -> P l1 l2.
  Proof.
    induction l1; intros l2 Hlen; simpl in Hlen; destruct l2; try inversion Hlen.
    - assumption.
    - apply Hcons; auto.
  Qed.
End list_pair_ind.

Section list_triple_ind.
  Variable A : Type.
  Variable B : Type.
  Variable C : Type.
  Variable P : list A -> list B -> list C -> Prop.

  Hypothesis Hnil : P nil nil nil.
  Hypothesis Hcons : forall tl1 tl2 tl3 a1 a2 a3,
      length tl1 = length tl2 ->
      length tl1 = length tl3 ->
      P tl1 tl2 tl3 -> P (a1::tl1) (a2::tl2) (a3::tl3).

  Theorem list_triple_ind : forall l1 l2 l3,
      length l1 = length l2 -> length l1 = length l3 -> P l1 l2 l3.
  Proof.
    induction l1; intros l2 l3 Hlen1 Hlen2; simpl in Hlen1; destruct l2; destruct l3; try inversion Hlen1; try inversion Hlen2.
    - assumption.
    - apply Hcons; auto.
  Qed.
End list_triple_ind.

Section list_hdhd_ins.
  Variable A : Type.
  Variable P : list A -> Prop.

  Hypothesis Hnil : P nil.
  Hypothesis Hconsnil : forall a, P (a::nil).
  Hypothesis Hcons : forall a1 a2 tl,
      P tl -> P (a2::tl) -> P (a1::a2::tl).

  Lemma list_hdhd_ins_aux : forall l1 a,
      P l1 /\ P (a::l1).
  Proof.
    induction l1 as [|a' l1]; intros a; auto.
    split; auto. apply IHl1.
    apply Hcons; apply IHl1. exact a.
  Qed.

  Theorem list_hdhd_ins : forall l, P l.
  Proof.
    intros l. destruct l.
    - auto.
    - apply list_hdhd_ins_aux.
  Qed.
End list_hdhd_ins.
