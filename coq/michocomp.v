(**                        Albert to Mi-Cho-Coq compiler                       *)

Require Michocoq.untyped_syntax Michocoq.syntax Michocoq.syntax_type Michocoq.typer Michocoq.untyper.
Require aux albert error label_lexico lexico_sort typer pretty_printers.

Import aux error.
Import albert typer  label_lexico lexico_sort pretty_printers.
Import Michocoq.untyped_syntax Michocoq.syntax_type.

Require Import List String Bool Ascii ZArith Permutation.
Require Import Coq.Program.Equality.


(* Type and value equivalence *)

Fixpoint michelson_type_of_type (t:ty) : M syntax_type.type :=
  match t with
  | ty_rty (rty_record lts) =>
    (fix michelson_type_of_rty lt :=
       match lt with
       | nil => Return _ syntax_type.unit
       | (_, le)::nil => (michelson_type_of_type le)
       | (_, le)::tl => let%err le' := michelson_type_of_type le in
                      let%err tl' := michelson_type_of_rty tl in
                      Return _ (syntax_type.pair le' tl')
       end) lts
  | ty_nat => Return _ (syntax_type.Comparable_type syntax_type.nat)
  | ty_int => Return _ (syntax_type.Comparable_type syntax_type.int)
  | ty_timestamp => Return _ (syntax_type.Comparable_type syntax_type.timestamp)
  | ty_mutez => Return _ (syntax_type.Comparable_type syntax_type.mutez)
  | ty_unit => Return _ syntax_type.unit
  | ty_string => Return _ (syntax_type.Comparable_type syntax_type.string)
  | ty_bytes => Return _ (syntax_type.Comparable_type syntax_type.bytes)
  | ty_prod ty1 ty2 =>
    let%err ty1' := michelson_type_of_type ty1 in
    let%err ty2' := michelson_type_of_type ty2 in
    Return _ (syntax_type.pair ty1' ty2')
  | ty_or ty1 ty2 =>
    let%err ty1' := michelson_type_of_type ty1 in
    let%err ty2' := michelson_type_of_type ty2 in
    Return _ (syntax_type.or ty1' ty2')
  | ty_option ty => let%err ty' := michelson_type_of_type ty in
                   Return _ (syntax_type.option ty')
  | ty_bool => Return _ (syntax_type.Comparable_type syntax_type.bool)
  | ty_vty (vty_variant ((s1, ty1)::(s2, ty2)::nil)) =>
    if (eqb_string s1 "None") && (eqb_string s2 "Some") && (type_eq ty1 ty_unit)
    then let%err ty2' := michelson_type_of_type ty2 in
         Return _ (syntax_type.option ty2')
    else if (eqb_string s1 "False") && (eqb_string s2 "True") && (type_eq ty1 ty_unit) && (type_eq ty2 ty_unit)
         then Return _ (syntax_type.Comparable_type syntax_type.bool)
         else let%err ty1' := michelson_type_of_type ty1 in
              let%err ty2' := michelson_type_of_type ty2 in
              Return _ (syntax_type.or ty1' ty2')
  | ty_vty (vty_variant lts) =>
    (fix michelson_type_of_vty lt :=
       match lt with
       | nil => Failed _ Should_not_happen
       | (_, le)::nil => (michelson_type_of_type le)
       | (_, le)::tl => let%err le' := michelson_type_of_type le in
                      let%err tl' := michelson_type_of_vty tl in
                      Return _ (syntax_type.or le' tl')
       end) lts
  | ty_list ty =>
    let%err ty' := michelson_type_of_type ty in
    Return _ (syntax_type.list ty')
  | ty_map ty1 ty2 =>
    let%err mty1 := michelson_type_of_type ty1 in
    let%err mty2 := michelson_type_of_type ty2 in
    match mty1 with
    | syntax_type.Comparable_type cty => Return _ (syntax_type.map cty mty2)
    | _ => Failed _ (Typing ("Key type of a map should be comparable, "++(string_of_type ty1)++" is not"))
    end
  | ty_big_map ty1 ty2 =>
    let%err mty1 := michelson_type_of_type ty1 in
    let%err mty2 := michelson_type_of_type ty2 in
    match mty1 with
    | syntax_type.Comparable_type cty => Return _ (syntax_type.big_map cty mty2)
    | _ => Failed _ (Typing ("Key type of a map should be comparable, "++(string_of_type ty1)++" is not"))
    end
  | ty_operation => Return _ syntax_type.operation
  | ty_key_hash => Return _ (syntax_type.Comparable_type syntax_type.key_hash)
  | ty_address => Return _ (syntax_type.Comparable_type syntax_type.address)
  | ty_contract ty => let%err ty' := michelson_type_of_type ty in
                     Return _ (syntax_type.contract ty')
  | ty_var _ => Failed _ Should_not_happen
  | ty_key => Return _ syntax_type.key
  | ty_signature => Return _ syntax_type.signature
  end.

(*
 * Set Printing All.
 *)

Fixpoint michelson_data_of_val_aux (v:value) : M untyped_syntax.concrete_data :=
  match v with
  | val_rval (rval_record lvs) =>
    (fix michelson_data_of_rval lv :=
       match lv with
       | nil => Return _ Unit
       | (_, le)::nil => (michelson_data_of_val_aux le)
       | (_, le)::tl =>
         let%err tle := michelson_data_of_val_aux le in
         let%err tri := michelson_data_of_rval tl in
         Return _ (Pair tle tri)
       end) lvs
  | val_num (numval_nat n) => Return _ (Int_constant (Z.of_N n))
  | val_num (numval_int n) => Return _ (Int_constant n)
  | val_num (numval_timestamp (timestampconst_int n)) => Return _ (Int_constant n)
  | val_num (numval_timestamp (timestampconst_string s)) => Return _ (String_constant s)
  | val_num (numval_mutez n) => Return _ (Int_constant (tez.to_Z n))
  | val_bool false => Return _ False_ | val_bool true => Return _ True_
  | val_string s => Return _ (String_constant s)
  | val_bytes b => Return _ (Bytes_constant b)
  | val_constr c v (vty_variant ((c1, ty1)::(c2, ty2)::nil)) =>
    let%err d := michelson_data_of_val_aux v in
    if (eqb_string c1 "None") && (eqb_string c2 "Some") && (type_eq ty1 ty_unit)
    then if (eqb_string c c1) then Return _ None_
         else Return _ (Some_ d)
    else if (eqb_string c1 "False") && (eqb_string c2 "True") && (type_eq ty1 ty_unit) && (type_eq ty2 ty_unit)
         then if (eqb_string c c1) then Return _ False_ else Return _ True_
         else if (eqb_string c c1) then Return _ (Left d) else Return _ (Right d)
  | val_constr c v vty =>
    let 'vty_variant vt := vty in
    (fix michelson_data_of_vval lt :=
       match lt with
       | nil => Failed _ Should_not_happen
       | (c', _)::nil => if (eqb_string c c') then (michelson_data_of_val_aux v) else Failed _ Should_not_happen
       | (c', _)::tl => if (eqb_string c c')
                      then let%err d := michelson_data_of_val_aux v in
                           Return _ (Left d)
                      else let%err d := michelson_data_of_vval tl in
                           Return _ (Right d)
       end) vt
  | val_lval (lval_list lv lt) =>
    let%err lv' :=
       ((fix michelson_data_of_lval lv :=
           match lv with
           | nil => Return _ nil
           | v::tl => let%err d := michelson_data_of_val_aux v in
                    let%err tl' := michelson_data_of_lval tl in
                    Return _ (d::tl')
           end) lv) in
    Return _ (Concrete_seq lv')
  | val_mval (mval_map lv ty1 ty2) =>
    let%err lv' :=
       (fix michelson_data_of_mval lv :=
          match lv with
          | nil => Return _ nil
          | (k, v)::tl => let%err dk := michelson_data_of_val_aux k in
                        let%err dv := michelson_data_of_val_aux v in
                        let%err tl' := michelson_data_of_mval tl in
                        Return _ ((Elt dk dv)::tl')
          end) lv in
    Return _ (Concrete_seq lv')
  | val_add (addval_tz tz) => Return _ (String_constant tz)
  | val_add (addval_kt kt) => Return _ (String_constant kt)
  | val_key k => Return _ (String_constant k)
  | val_signature s => Return _ (String_constant s)
  end.

Definition michelson_data_of_val (v:value) : M untyped_syntax.concrete_data :=
  let sval := match v with
              | val_rval (rval_record lv) => val_rval (rval_record (LexicoValSort.sort lv))
              | _ => v
              end in
  (michelson_data_of_val_aux sval).

(* Stack manipulation *)
Fixpoint index_and_remove (x : label) (l : list label) (n : Datatypes.nat) : M (Datatypes.nat * list label) :=
  match l with
  | nil => Failed _ Should_not_happen
  | hd::tl => if string_dec x hd then Return _ (n, tl)
            else let%err (i, tl') := index_and_remove x tl (S n) in
                 Return _ (i, hd::tl')
  end.

Lemma index_and_remove_ge : forall x l l' n k,
    index_and_remove x l n = Return _ (k, l') ->
    k >= n.
Proof.
  induction l; intros l' n k Hind; simpl in *.
  - discriminate Hind.
  - destruct (string_dec x a).
    + invert Hind. omega.
    + apply bind_eq_return in Hind as [[n' l''] [Hind1 Hind2]].
      invert Hind2.
      specialize (IHl _ _ _ Hind1). omega.
Qed.

Lemma index_and_remove_S: forall x l l' n k,
    index_and_remove x l (S n) = Return _ ((S k), l') <->
    index_and_remove x l n = Return _ (k, l').
Proof.
  induction l; intros l' n k; simpl in *; split; intro H; try inversion H.
  - destruct (string_dec x a).
    + invert H; reflexivity.
    + apply bind_eq_return in H as [[n' l''] [Hind1 Hind2]].
      invert Hind2.
      rewrite (IHl _ _ _) in Hind1. rewrite Hind1. reflexivity.
  - destruct (string_dec x a).
    + invert H. reflexivity.
    + clear H1. apply bind_eq_return in H as [[n' l''] [Hret1 Hret2]].
      invert Hret2.
      specialize (IHl l'' (S n) k). rewrite <- IHl in Hret1.
      rewrite Hret1. reflexivity.
Qed.

Lemma index_and_remove_nth: forall x l l' n,
    index_and_remove x l 0 = Return _ (n, l') ->
    nth_error l n = Some x.
Proof.
  induction l; intros l' n Hret; simpl in *.
  - discriminate Hret.
  - destruct (string_dec x a).
    + subst; invert Hret; auto.
    + apply bind_eq_return in Hret as [[n' l''] [Hret1 Hret2]]; invert Hret2.
      destruct n; simpl in *.
      * apply index_and_remove_ge in Hret1. omega.
      * specialize (index_and_remove_S x l l'' 0 n) as Hret'.
        rewrite Hret' in Hret1.
        specialize (IHl l'' n) as IHl.
        simpl; auto.
Qed.

Lemma index_and_remove_perm: forall x l l' n,
    index_and_remove x l 0 = Return _ (n, l') ->
    Permutation l (x::l').
Proof.
  induction l; intros l' n Hret; simpl in *.
  - discriminate Hret.
  - destruct (string_dec x a).
    + subst. invert Hret. auto.
    + apply bind_eq_return in Hret as [[ty' l''] [Hret1 Hret2]].
      invert Hret2.
      eapply perm_trans. 2: apply perm_swap.
      destruct n.
      * apply index_and_remove_ge in Hret1; omega.
      * constructor. apply IHl with n; auto.
        apply index_and_remove_S; auto.
Qed.

Lemma index_and_remove_spec: forall x l1 l2,
    ~(In x l1) ->
    index_and_remove x (l1++x::l2) 0 = Return _ (List.length l1, (l1++l2)).
Proof.
  induction l1; intros l2 HnIn; simpl in *.
  - subst.
    destruct string_dec; try contradiction. reflexivity.
  - destruct string_dec.
    + subst. exfalso. auto.
    + specialize (IHl1 l2).
      specialize (index_and_remove_S x (l1++x::l2) (l1++l2) 0 (Datatypes.length l1)) as Hret'.
      rewrite <- Hret' in IHl1. rewrite IHl1. reflexivity.
      intuition.
Qed.

(** Dig a variable from the stack and
    puts the variable on top of the representation of the stack *)
Definition compute_dig (consumed : label) (stack : list label) :=
  let%err (i, stack'):= index_and_remove consumed stack 0 in
  Return _ (DIG i, consumed::stack').

Lemma compute_dig_spec: forall x l1 l2,
    ~(In x l1) ->
    compute_dig x (l1++x::l2) = Return _ (DIG (List.length l1), (x::l1++l2)).
Proof.
  intros x l1 l2 HnIn.
  unfold compute_dig.
  rewrite index_and_remove_spec; auto.
Qed.

(** Return the nth-tail, that is cut off the n first elements of the list
    Generally used after compute_dig *)
Fixpoint tln stack n : list label :=
  match stack, n with
  | nil, _ => nil
  | _, 0 => stack
  | hd::tl, S n' => tln tl n'
  end.

(** Compile an argument *)
Definition compile_arg (arg:arg) (stack: list label) : M (untyped_syntax.instruction * list label) :=
  match arg with
  | arg_var var => let%err (ins, stack')  := compute_dig var stack in
                  Return _ (ins, tln stack' 1)
  | arg_value val => let%err d := michelson_data_of_val val in
                    let%err t := type_value val in
                    let%err mty := michelson_type_of_type t in
                    match mty with
                    | syntax_type.list mty' =>
                      match val with
                      | (val_lval (lval_list nil _)) => Return _ (NIL mty', stack)
                      | _ => Return _ (PUSH mty d, stack)
                      end
                    | _ => Return _ (PUSH mty d, stack)
                    end
  | arg_record vars => let%err (ins, stack') :=
                         (fix michelson_pair_of_rec vars :=
                            match vars with
                            | nil => Return _ (NOOP, stack)
                            | (_, hd)::nil => let%err (ins, stack')  := compute_dig hd stack in
                                            Return _ (ins, stack')
                            | (_, hd)::tl =>
                              let%err (ins1, stack')  := michelson_pair_of_rec tl in
                              let%err (ins2, stack'') := compute_dig hd stack' in
                              Return _ (SEQ (SEQ ins1 ins2) PAIR, tln stack'' 1)
                            end)  (LexicoLabSort.sort vars) in
                      Return _ (ins, tln stack' 1)
  end.

Definition function : Type := (fvar * syntax_type.type * untyped_syntax.instruction * syntax_type.type).

Definition DROP1 := DROP 1.
Definition DIP1 := DIP 1.

(** Compile a function *)
Definition compile_funct f defs : M (untyped_syntax.instruction) :=
  match f with
  | fun_var fv => (fix find_fvar (defs: list function) :=
                    match defs with
                    | nil => Failed _ Should_not_happen
                    | (fv', _, ins, _)::tl => if (eqb_string fv fv') then
                                              Return _ ins
                                            else (find_fvar tl)
                    end) defs
  | fun_dup => Return _ (SEQ DUP PAIR)
  | fun_constr c (vty_variant ((c1, t1)::(c2, t2)::nil)) => (* Particular cases *)
    if (eqb_string c1 "None") && (eqb_string c2 "Some") && (type_eq t1 ty_unit) (* Option *)
    then if (eqb_string c c1)
         then let%err t2' := michelson_type_of_type t2 in
              Return _ (SEQ DROP1 (NONE t2'))
         else Return _ SOME
    else if (eqb_string c1 "False")
              && (eqb_string c2 "True")
              && (type_eq t1 ty_unit) && (type_eq t2 ty_unit) (* Bool *)
         then if (eqb_string c c1)
              then Return _ (SEQ DROP1 (PUSH (syntax_type.Comparable_type syntax_type.bool) False_))
              else Return _ (SEQ DROP1 (PUSH (syntax_type.Comparable_type syntax_type.bool) True_))
         else if (eqb_string c c1)  (* General case *)
              then let%err t2' := michelson_type_of_type t2 in
                   Return _ (LEFT t2')
              else let%err t1' := michelson_type_of_type t1 in
                   Return _ (RIGHT t1')
  | fun_constr c vty =>
    let 'vty_variant vt := vty in
    (fix compile_variant vt :=
       match vt with
       | nil => Failed _ Should_not_happen
       | (c', t)::nil => if (eqb_string c c') then Return _ NOOP else Failed _ Should_not_happen
       | (c', t)::tl => if (eqb_string c c')
                      then let%err t' :=
                              michelson_type_of_type (ty_vty (vty_variant tl)) in
                           Return _ (LEFT t')
                      else let%err ins := compile_variant tl in
                           let%err t' := michelson_type_of_type t in
                           Return _ (SEQ ins (RIGHT t'))
       end) vt
  | fun_contract ty => let%err ty' := michelson_type_of_type ty in
                      Return _ (CONTRACT ty')
  | fun_address => Return _ ADDRESS
  | fun_implicit_account => Return _ IMPLICIT_ACCOUNT
  | fun_hash_key => Return _ HASH_KEY
  | fun_blake2b => Return _ BLAKE2B
  | fun_sha256 => Return _ SHA256
  | fun_sha512 => Return _ SHA512
  end.

Fixpoint generate_cdrs (l:label) (lt : list (label * ty)) : M untyped_syntax.instruction :=
  match lt with
  | nil => Failed _ Should_not_happen
  | (l', _)::nil => if (eqb_string l l') then Return _ NOOP else Failed _ Should_not_happen
  | (l', _)::tl => if (eqb_string l l')
                  then Return _ CAR
                 else let%err ins := generate_cdrs l tl in
                      Return _ (SEQ CDR ins)
  end.

Fixpoint generate_unpairs (ls : list (label * label)) (ls' : list (label * ty)) (stack : list label) : M (untyped_syntax.instruction * list label) :=
  match ls, ls' with
  | nil, _ => Return _ (NOOP, stack)
  | _, nil => Failed _ Should_not_happen
  | (l, x)::nil, (l', _)::nil =>
    if (eqb_string l l')
    then let%err (dig, stack') := compute_dig x (tln stack 1) in
         Return _ (SEQ DROP1 dig, stack')
    else Failed _ Should_not_happen
  | (l, x)::tl, (l', _)::tl' =>
    if (eqb_string l l')
    then let%err (dig, stack')  := compute_dig x stack in
         let%err (ins, stack'') := generate_unpairs tl tl' (tln stack' 1) in
         Return _ (SEQ UNPAIR (SEQ DROP1 (SEQ dig (SEQ (DIP1 ins) PAIR))), stack'')
    else let%err (ins, stack') := generate_unpairs ((l, x)::tl) tl' stack in
         Return _ (SEQ UNPAIR (SEQ (DIP1 ins) PAIR), stack') (* TODO pb here *)
  end.

(** Compile a right-hand side *)
Fixpoint compile_rhs (rhs:rhs) (stack: list label) (stackType : list (label * ty)) (defs: list function) : M (untyped_syntax.instruction * list label) :=
  match rhs with
  | rhs_arg arg => compile_arg arg stack
  | rhs_app f arg =>
    let%err (ins1, stack') :=compile_arg arg stack in
    let%err ins2 := compile_funct f defs in
    Return _ (SEQ ins1 ins2, stack')
  | rhs_projection var lab =>
    let%err (dig, stack') := compute_dig var stack in
    let%err (t, _) := type_and_remove var stackType in
    match t with
    | ty_rty (rty_record lt) =>
      let%err ins := generate_cdrs lab lt in
      Return _ (SEQ dig ins, tln stack' 1)
    | _ => Failed _ Should_not_happen
    end
  | rhs_update var lx =>
    let slx := (LexicoLabSort.sort lx) in
    let%err (dig, stack') := compute_dig var stack in
    let%err (t, _) := type_and_remove var stackType in
    match t with
    | ty_rty (rty_record lt) =>
      let%err (unpairs, stack'') := generate_unpairs slx lt stack' in
      Return _ (SEQ dig unpairs, tln stack'' 1)
    | _ => Failed _ Should_not_happen
    end
  | rhs_add x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) ADD, tln stack'' 2)
  | rhs_sub x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) SUB, tln stack'' 2)
  | rhs_mul x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) MUL, tln stack'' 2)
  | rhs_ediv x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) EDIV, tln stack'' 2)
  | rhs_not x =>
    let%err (dig, stack') := compute_dig x stack in
    Return _ ((SEQ dig NOT), tln stack' 1)
  | rhs_and x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) AND, tln stack'' 2)
  | rhs_or x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) OR, tln stack'' 2)
  | rhs_xor x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) XOR, tln stack'' 2)
  | rhs_lsl x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) LSL, tln stack'' 2)
  | rhs_lsr x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) LSR, tln stack'' 2)
  | rhs_abs x =>
    let%err (dig, stack') := compute_dig x stack in
    Return _ (SEQ dig ABS, tln stack' 1)
  | rhs_is_nat x =>
    let%err (dig, stack') := compute_dig x stack in
    Return _ (SEQ dig ISNAT, tln stack' 1)
  | rhs_int x =>
    let%err (dig, stack') := compute_dig x stack in
    Return _ (SEQ dig INT, tln stack' 1)
  | rhs_match x (branchesrhs_branches lbr) =>
    let%err (dig1, stack') := compute_dig x stack in
    let stack' := tln stack' 1 in
    let%err (tvar, stackType') := type_and_remove x stackType in
    match tvar with
    | ty_vty (vty_variant vt) =>
      (fix compile_branches vt lbr {struct lbr}:=
         match vt, lbr with
         | nil, _ => Failed _ Should_not_happen
         | _, nil => Failed _ Should_not_happen
         | (_, t)::_ , (pattern_variant c l, rhs)::nil => compile_rhs rhs (l::stack') ((l, t)::stackType') defs
         | (_, t)::_ , (pattern_any, rhs)::_ =>
           let%err (ins,stack'') := compile_rhs rhs stack' stackType' defs in
           Return _ (SEQ dig1 (SEQ DROP1 ins), stack'')
         | (_, t)::tl1, (pattern_variant c l, rhs)::tl2 =>
           let%err (insl,_) := (compile_rhs rhs (l::stack') ((l, t)::stackType') defs) in
           let%err (insr, stack'') := (compile_branches tl1 tl2) in
           Return _ (SEQ dig1 (IF_LEFT insl insr), stack'')
         | _, _ => Failed _ Should_not_happen
         end) vt lbr
    | ty_or ty1 ty2 =>
      match lbr with
      | (pattern_variant _ l1, rhs1)::(pattern_variant _ l2, rhs2)::nil =>
        let%err (rhs1', _) := compile_rhs rhs1 (l1::stack') ((l1, ty1)::stackType') defs in
        let%err (rhs2', stack'') := compile_rhs rhs2 (l2::stack') ((l2, ty2)::stackType') defs in
        Return _ (SEQ dig1 (IF_LEFT rhs1' rhs2'), stack'')
      | _ => Failed _ Should_not_happen
      end
    | ty_option ty =>
      match lbr with
      | (pattern_variant _ l1, rhs1)::(pattern_variant _ l2, rhs2)::nil =>
        let%err (rhs1', _) := compile_rhs rhs1 (l1::stack') ((l1, ty_unit)::stackType') defs in
        let%err (rhs2', stack'') := compile_rhs rhs2 (l2::stack') ((l2, ty)::stackType') defs in
        Return _ (SEQ dig1 (IF_NONE (SEQ (PUSH syntax_type.unit Unit) rhs1') rhs2'), stack'')
      | _ => Failed _ Should_not_happen
      end
    | ty_bool =>
      match lbr with
      | (pattern_variant _ l1, rhs1)::(pattern_variant _ l2, rhs2)::nil =>
        let%err (rhs1', _) := compile_rhs rhs1 (l1::stack') ((l1, ty_unit)::stackType') defs in
        let%err (rhs2', stack'') := compile_rhs rhs2 (l2::stack') ((l2, ty_unit)::stackType') defs in
        Return _ (SEQ dig1 (IF_ (SEQ (PUSH syntax_type.unit Unit) rhs1') (SEQ (PUSH syntax_type.unit Unit) rhs2')), stack'')
      | _ => Failed _ Should_not_happen
      end
    | ty_list ty =>
      match lbr with
      | (pattern_nil , rhs_nil)::(pattern_cons hd tl, rhs_cons)::nil =>
        let%err (rhs_nil',_) := compile_rhs rhs_nil stack' stackType' defs in
        let%err (rhs_cons',stack'') :=
           compile_rhs rhs_cons (hd::tl::stack') ((hd,ty)::(tl, ty_list ty)::stackType') defs in
        Return _ (SEQ dig1 (IF_CONS rhs_cons' rhs_nil'),stack'')
      | (pattern_nil , rhs_nil)::(pattern_any, rhs_any)::nil =>
        let%err (rhs_nil',_) := compile_rhs rhs_nil stack' stackType' defs in
        let%err (rhs_any',stack'') := compile_rhs rhs_any stack' stackType' defs in
        Return _ (SEQ dig1 (IF_CONS (SEQ (DROP 2) rhs_any') rhs_nil' ), stack'')
      | (pattern_cons hd tl, rhs_cons)::(pattern_any , rhs_any)::nil =>
        let%err (rhs_any',_) := compile_rhs rhs_any stack' stackType' defs in
        let%err (rhs_cons',stack'') :=
           compile_rhs rhs_cons (hd::tl::stack') ((hd,ty)::(tl, ty_list ty)::stackType') defs in
        Return _ (SEQ dig1 (IF_CONS rhs_cons' (SEQ (DROP 2) rhs_any')),stack'')
      | _ => Failed _ Should_not_happen
      end
    | _ => Failed _ Should_not_happen
    end
  | rhs_concat x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) CONCAT, tln stack'' 2)
  | rhs_size x =>
    let%err (dig, stack') := compute_dig x stack in
    Return _ (SEQ dig SIZE, tln stack' 1)
  | rhs_slice x1 x2 x3 =>
    let%err (dig1, stack') := compute_dig x1 stack in
    let%err (dig2, stack'') := compute_dig x2 stack' in
    let%err (dig3, stack''') := compute_dig x3 stack'' in
    Return _ ((SEQ (SEQ (SEQ dig1 dig3) dig2) SLICE), tln stack''' 3)
  | rhs_bytes_pack x => let%err (dig, stack') := compute_dig x stack in
                       Return _ (SEQ dig PACK, tln stack' 1)
  | rhs_bytes_unpack t x =>
    let%err (dig, stack') := compute_dig x stack in
    let%err mty := michelson_type_of_type t in
    Return _ (SEQ dig (UNPACK mty), tln stack' 1)
  | rhs_eq x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE EQ), tln stack'' 2)
  | rhs_neq x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE NEQ), tln stack'' 2)
  | rhs_inf x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE LT), tln stack'' 2)
  | rhs_infeq x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE LE), tln stack'' 2)
  | rhs_sup x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE GT), tln stack'' 2)
  | rhs_supeq x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) (SEQ COMPARE GE), tln stack'' 2)
  | rhs_list l =>
    let%err (ins, st) := (fix compile_rhs_list l st :=
                            match l with
                            | nil => Failed _ Should_not_happen
                            | hd::nil => let%err (ty,_) := type_and_remove hd stackType in
                                       let%err mty := michelson_type_of_type ty in
                                       let%err (dig, st') := compute_dig hd (""::st) in
                                       Return _ ((SEQ (NIL mty) (SEQ dig CONS)), (tln st' 1))
                            | hd::tl => let%err (ins, st') := compile_rhs_list tl st in
                                      let%err (dig, st'') := compute_dig hd st' in
                                      Return _ ((SEQ ins (SEQ dig CONS)), (tln st'' 1))
                            end) l stack in
    Return _ (ins, tln st 1)
  | rhs_list_cons x1 x2 =>
    let%err (dig1, stack') := compute_dig x2 stack in
    let%err (dig2, stack'') := compute_dig x1 stack' in
    Return _ (SEQ (SEQ dig1 dig2) CONS, tln stack'' 2)
  | rhs_list_map l1 l2 r =>
    let%err (dig1, stack') := compute_dig l1 stack in
    let%err (ty, stackType') := type_and_remove l1 stackType in
    match ty with
    | ty_list ty' => let%err (ins, stack'') :=
                       compile_rhs r (l2::(tln stack' 1)) ((l2, ty')::stackType) defs in
                    Return _ (SEQ dig1 (MAP ins), stack'')
    | _ => Failed _ Should_not_happen
    end
  | rhs_map vars =>
     let%err (ins, st') := (fix compile_rhs_list l st :=
        match l with
        | nil => Failed _ Should_not_happen
        | (l1, l2)::nil =>
          let%err (ty1, _) := type_and_remove l1 stackType in
          let%err (ty2, _) := type_and_remove l2 stackType in
          let%err mty1 := michelson_type_of_type ty1 in
          let%err mty2 := michelson_type_of_type ty2 in
          match mty1 with
          | syntax_type.Comparable_type mty1' =>
            let%err (dig1, st') := compute_dig l2 (""::st) in
            let%err (dig2, st'') := compute_dig l1 st' in
            Return _ (SEQ (EMPTY_MAP mty1' mty2)
                          (SEQ (SEQ (SEQ dig1 SOME) dig2) UPDATE), (tln st'' 2))
          | _ => Failed _ (Typing ("key type of map should be comparable, "++(string_of_type ty1)++"is not"))
          end
        | (l1, l2)::tl =>
          let%err (ins, st') := compile_rhs_list tl st in
          let%err (dig1, st'') := compute_dig l2 st' in
          let%err (dig2, st''') := compute_dig l1 st'' in
          Return _ (SEQ ins (SEQ (SEQ (SEQ dig1 SOME) dig2) UPDATE), (tln st''' 2))
        end) vars stack in
     Return _ (ins, tln st' 1)
  | rhs_map_map l1 l2 var r =>
    let%err (dig1, stack') := compute_dig var stack in
    let%err (ty, stackType') := type_and_remove var stackType in
    match ty with
    | ty_map ty1 ty2 => let%err (ins, stack'') :=
                          compile_rhs r (l1::l2::(tln stack' 1)) ((l1, ty1)::(l2, ty2)::stackType) defs in
                       Return _ (SEQ dig1 (MAP (SEQ UNPAIR ins)), stack'')
    | _ => Failed _ Should_not_happen
    end
  | rhs_map_get var l =>
    let%err (dig1, stack') := compute_dig var stack in
    let%err (dig2, stack'') := compute_dig l stack' in
    Return _ (SEQ (SEQ dig1 dig2) GET, tln stack'' 2)
  | rhs_map_update var l1 l2 =>
    let%err (dig1, stack') := compute_dig var stack in
    let%err (dig2, stack'') := compute_dig l2 stack' in
    let%err (dig3, stack''') := compute_dig l1 stack'' in
    Return _ (SEQ (SEQ dig1 (SEQ dig2 dig3)) UPDATE, tln stack''' 3)
  | rhs_check_signature var_k var_sig var_by =>
    let%err (dig1, stack') := compute_dig var_by stack in
    let%err (dig2, stack'') := compute_dig var_sig stack' in
    let%err (dig3, stack''') := compute_dig var_k stack'' in
    Return _ (SEQ (SEQ dig1 (SEQ dig2 dig3)) CHECK_SIGNATURE, tln stack''' 3)
  | rhs_amount => Return _ (AMOUNT, stack)
  | rhs_self => Return _ (SELF, stack)
  | rhs_transfer_tokens var_p var_am var_dest =>
    let%err (dig1, stack') := compute_dig var_dest stack in
    let%err (dig2, stack'') := compute_dig var_am stack' in
    let%err (dig3, stack''') := compute_dig var_p stack'' in
    Return _ (SEQ (SEQ dig1 (SEQ dig2 dig3)) TRANSFER_TOKENS, tln stack''' 3)
  | rhs_set_delegate var =>
    let%err (dig, stack') := compute_dig var stack in
    Return _ (SEQ dig SET_DELEGATE, tln stack' 1)
  | rhs_sender => Return _ (SENDER, stack)
  | rhs_source => Return _ (SOURCE, stack)
  | rhs_now => Return _ (NOW, stack)
  end.

Fixpoint index_and_add (x : label) (l : list label) (n : Datatypes.nat) : Datatypes.nat * list label :=
  match l with
  | nil => (n, x::nil)
  | hd::tl => if lexico_leb x hd then (n, x::hd::tl)
            else let '(n, tl') := (index_and_add x tl (S n)) in (n, hd::tl')
  end.

(** Compile a left-hand side *)
Definition compile_lhs (lhs:lhs) (stack: list label) (t: ty) : M (untyped_syntax.instruction * list label) :=
  match lhs with
  | lhs_var var =>
    let (n, stack') := index_and_add var stack 0 in Return _ (DUG n, stack')
  | lhs_record lvars =>
    match t with
    | ty_rty (rty_record lt) =>
      (fix generate_binds lvars stack :=
         match lvars with
         | nil => Return _ (DROP1, stack)
         | (l, var)::nil => let (n, stack') := (index_and_add var stack 0) in
                          Return _ (DUG n, stack')
         | (l, var)::tl =>
           let (n, stack') := (index_and_add var stack 1) in
           let%err (ins1, stack'') := generate_binds tl stack' in
           Return _ (SEQ UNPAIR (SEQ (DUG n) ins1), stack'')
         end) lvars stack
    | ty_prod ty1 ty2 =>
      match lvars with
      | (_, x1)::(_, x2)::nil =>
        let (n1, stack') := index_and_add x1 stack 1 in
        let (n2, stack'') := index_and_add x2 stack' 0 in
        Return _ (SEQ UNPAIR (SEQ (DUG n1) (DUG n2)), stack'')
      | _ => Failed _ Should_not_happen
      end
    | _ => Failed _ Should_not_happen
    end
  end.

Open Scope nat_scope.

(** Compile an instruction *)
Fixpoint compile_instr (ins:typed_instr) (stack : list label) (defs: list function) : M (untyped_syntax.instruction * list label) :=
  match ins with
  | ty_instr_noop _ => Return _ (NOOP, stack)
  | ty_instr_seq _ i1 i2 _ =>
    let%err (ins1, stack') := compile_instr i1 stack defs in
    let%err (ins2, stack'') := compile_instr i2 stack' defs in
    Return _ (SEQ ins1 ins2, stack'')
  | ty_instr_assign rt lhs t rhs _ =>
    let '(rty_record lt) := rt in
    let%err (ins1, stack') := compile_rhs rhs stack lt defs in
    let%err (ins2, stack'') := compile_lhs lhs stack' t in
    Return _ (SEQ ins1 ins2, stack'')
  | ty_instr_drop _ var _ =>
    let%err (dig, stack') := compute_dig var stack in
    Return _ (SEQ dig DROP1, tln stack' 1)
  | ty_instr_match stackType x lbr _ =>
    let 'rty_record stackType := stackType in
    let%err (dig1, stack') := compute_dig x stack in
    let stack' := tln stack' 1 in
    let%err (tvar, stackType') := type_and_remove x stackType in
    match tvar with
    | ty_vty (vty_variant vt) =>
      (fix compile_branches vt lbr returnStack {struct lbr} :=
         match vt, lbr with
         | nil, _ => Failed _ Should_not_happen
         | _, nil => Failed _ Should_not_happen
         |(_, t)::_,(pattern_variant c l, ins)::nil  =>
          let (n, stack'') := index_and_add l stack' 0 in
          let%err (ins', stack''') := (compile_instr ins stack'' defs) in
          Return _ (SEQ (DUG n) ins',
                    if (List.length returnStack =? 0) then stack''' else returnStack) (* suspiscious *)
         |(_, _ )::_,(pattern_any, ins)::_  =>
          let%err (ins', stack'') := (compile_instr ins stack' defs) in
          Return _ (SEQ DROP1 ins',
                    if (List.length returnStack =? 0) then stack'' else returnStack) (* suspiscious *)
         | (_, t)::tl1,(pattern_variant c l, ins)::tl2 =>
           let (n, stack'') := index_and_add l stack' 0 in
           let%err (insl, stackl) := compile_instr ins stack'' defs in
           let%err (insr, stackr) :=
              compile_branches
                tl1 tl2
                (if (List.length returnStack =? 0) then stackl else returnStack) in
           Return _ (SEQ dig1 (IF_LEFT (SEQ (DUG n) insl) insr), stackr)
         | _::_,_ => Failed _ Should_not_happen
         end) vt lbr nil
    | ty_or _ _ =>
      match lbr with
      | (pattern_variant _ l1, ins1)::(pattern_variant _ l2, ins2)::nil =>
        let (n1, stack1) := index_and_add l1 stack' 0 in
        let (n2, stack2) := index_and_add l2 stack' 0 in
        let%err (ins1', stack1') := compile_instr ins1 stack1 defs in
        let%err (ins2', stack2') := compile_instr ins2 stack2 defs in
        Return _ (SEQ dig1 (IF_LEFT (SEQ (DUG n1) ins1') (SEQ (DUG n2) ins2')),
                  if (List.length stack1' =? 0) then stack2' else stack1')
      | _ => Failed _ Should_not_happen
      end
    | ty_option _ =>
      match lbr with
      | (pattern_variant _ l1, ins1)::(pattern_variant _ l2, ins2)::nil =>
        let (n1, stack1) := index_and_add l1 stack' 0 in
        let (n2, stack2) := index_and_add l2 stack' 0 in
        let%err (ins1', stack1') := compile_instr ins1 stack1 defs in
        let%err (ins2', stack2') := compile_instr ins2 stack2 defs in
        Return _ (SEQ dig1 (IF_NONE (SEQ (SEQ (PUSH syntax_type.unit Unit) (DUG n1)) ins1')
                                    (SEQ (DUG n2) ins2')),
                  if (List.length stack1' =? 0) then stack2' else stack1')
      | _ => Failed _ Should_not_happen
      end
    | ty_bool =>
      match lbr with
      | (pattern_variant _ l1, ins1)::(pattern_variant _ l2, ins2)::nil =>
        let (n1, stack1) := index_and_add l1 stack' 0 in
        let (n2, stack2) := index_and_add l2 stack' 0 in
        let%err (ins1', stack1') := compile_instr ins1 stack1 defs in
        let%err (ins2', stack2') := compile_instr ins2 stack2 defs in
        Return _ (SEQ dig1 (IF_ (SEQ (SEQ (PUSH syntax_type.unit Unit) (DUG n2)) ins2')
                                (SEQ (SEQ (PUSH syntax_type.unit Unit) (DUG n1)) ins1')),
                  if (List.length stack1' =? 0) then stack2' else stack1')
      | _ => Failed _ Should_not_happen
      end
    | ty_list ty =>
      match lbr with
      | (pattern_nil , ins_nil)::(pattern_cons hd tl, ins_cons)::nil =>
        let%err (ins_nil',_) := compile_instr ins_nil stack' defs in
        let (nhd, stackhd ) := index_and_add hd stack' 1 in
        let (ntl, stack2) := index_and_add tl stackhd 0 in
        let%err (ins_cons',stack'') := compile_instr ins_cons stack2 defs in
        Return _ (SEQ dig1 (IF_CONS (SEQ (SEQ (DUG nhd) (DUG ntl)) ins_cons') ins_nil'),stack'')
      | (pattern_nil , ins_nil)::(pattern_any, ins_any)::nil =>
        let%err (ins_nil',_) := compile_instr ins_nil stack' defs in
        let%err (ins_any',stack'') := compile_instr ins_any stack' defs in
        Return _ (SEQ dig1 (IF_CONS (SEQ (DROP 2) ins_any') ins_nil' ), stack'')
      | (pattern_cons hd tl , ins_cons)::(pattern_any , ins_any)::nil =>
        let%err (ins_any',_) := compile_instr ins_any stack' defs in
        let (nhd, stackhd ) := index_and_add hd stack' 1 in
        let (ntl, stack2) := index_and_add tl stackhd 0 in
        let%err (ins_cons',stack'') := compile_instr ins_cons stack2 defs in
        Return _ (SEQ dig1 (IF_CONS (SEQ (SEQ (DUG nhd) (DUG ntl)) ins_cons') ins_any'),stack'')
      | _ => Failed _ Should_not_happen
      end
    | _ => Failed _ Should_not_happen
    end
  | ty_instr_loop_left _ var var' body _ =>
    let%err (dig, stack') := compute_dig var stack in
    let stack' := tln stack' 1 in
    let '(n, stack'') := index_and_add var' stack' 0 in
    let%err (ibody, stack''') := compile_instr body stack'' defs in
    let%err (dig', _) := compute_dig var stack''' in
    Return _ (SEQ (SEQ dig (LOOP_LEFT (SEQ (DUG n) (SEQ ibody dig')))) DROP1, stack')
  | ty_instr_loop _ var body _ =>
    let%err (dig, stack') := compute_dig var stack in
    let stack' := tln stack' 1 in
    let%err (ibody, stack'') := compile_instr body stack' defs in
    let%err (dig', _) := compute_dig var stack'' in
    Return _ (SEQ dig (LOOP (SEQ ibody dig')), stack')
  | ty_instr_list_for _ var' var body _ =>
    let%err (dig, stack') := compute_dig var stack in
    let stack' := tln stack' 1 in
    let '(n, stack'') := index_and_add var' stack' 0 in
    let%err (ibody, _) := compile_instr body stack'' defs in
    Return _ (SEQ dig (ITER ibody), stack')
  | ty_instr_map_for _ l1 l2 var body _ =>
    let%err (dig, stack') := compute_dig var stack in
    let stack' := tln stack' 1 in
    let '(n1, stack1) := index_and_add l1 stack' 1 in
    let '(n2, stack2) := index_and_add l2 stack1 0 in
    let%err (ibody, _) := compile_instr body stack2 defs in
    Return _ (SEQ dig (ITER (SEQ UNPAIR ibody)), stack')
  | ty_instr_failwith _ a =>
    let%err (ins, stack') := compile_arg a stack in
    Return _ (SEQ ins FAILWITH, nil)
  end.

Definition type_and_compile (ins:albert.instruction) (startType : rty) (startStack : list label) (defs : list function) : M (untyped_syntax.instruction * list label) :=
  let%err ty_ins := typed_instr_of_instr ins startType g_empty in
  compile_instr ty_ins startStack defs.

(* Optimizations *)

(** Change all the "DIG 0" and "DUG 0" instructions to NOOP (to be removed later) *)
Fixpoint dig0dug0 (ins:untyped_syntax.instruction) : untyped_syntax.instruction :=
  match ins with
  | DIG 0 | DUG 0 => NOOP
  | SEQ i1 i2 => SEQ (dig0dug0 i1) (dig0dug0 i2)
  | DIP 1 i => DIP 1 (dig0dug0 i)
  | IF_ i1 i2 => IF_ (dig0dug0 i1) (dig0dug0 i2)
  | IF_NONE i1 i2 => IF_NONE (dig0dug0 i1) (dig0dug0 i2)
  | IF_LEFT i1 i2 => IF_LEFT (dig0dug0 i1) (dig0dug0 i2)
  | IF_CONS i1 i2 => IF_CONS (dig0dug0 i1) (dig0dug0 i2)
  | LOOP i => LOOP (dig0dug0 i)
  | LOOP_LEFT i => LOOP_LEFT (dig0dug0 i)
  | ITER i => ITER (dig0dug0 i)
  | MAP i => MAP (dig0dug0 i)
  | _ => ins
  end.

(** Return a pair with the leftMost instruction and the rest *)
Fixpoint leftMost ins : (untyped_syntax.instruction * untyped_syntax.instruction) :=
  match ins with
  | SEQ i1 i2 => let '(le, rest) := leftMost i1 in (le, SEQ rest i2)
  | _ => (ins, NOOP)
  end.

(** Return a pair with the rest and the rightMost instruction *)
Fixpoint rightMost ins : (untyped_syntax.instruction * untyped_syntax.instruction) :=
  match ins with
  | SEQ i1 i2 => let '(rest, ri) := rightMost i2 in (SEQ i1 rest, ri)
  | _ => (NOOP, ins)
  end.

Fixpoint digndugn ins : untyped_syntax.instruction :=
  match ins with
  | SEQ i1 i2 =>
    let i1' := (digndugn i1) in
    let i2' := (digndugn i2) in
    let '(i1'', ri) := rightMost i1' in
    let '(le, i2'') := leftMost i2' in
    match le, ri with
    | DIG n1, DUG n2 | DUG n1, DIG n2 =>
                       if (n1 =? n2) then SEQ i1'' i2'' else SEQ i1' i2'
    | _, _ => SEQ i1' i2'
    end
  | DIP 1 i => DIP 1 (digndugn i)
  | IF_ i1 i2 => IF_ (digndugn i1) (digndugn i2)
  | IF_NONE i1 i2 => IF_NONE (digndugn i1) (digndugn i2)
  | IF_LEFT i1 i2 => IF_LEFT (digndugn i1) (digndugn i2)
  | IF_CONS i1 i2 => IF_CONS (digndugn i1) (digndugn i2)
  | LOOP i => LOOP (digndugn i)
  | LOOP_LEFT i => LOOP_LEFT (digndugn i)
  | ITER i => ITER (digndugn i)
  | MAP i => MAP (digndugn i)
  | _ => ins
  end.

Fixpoint diug1diug1 ins : untyped_syntax.instruction :=
  match ins with
  | SEQ i1 i2 =>
    let i1' := (diug1diug1 i1) in
    let i2' := (diug1diug1 i2) in
    let '(i1'', ri) := rightMost i1' in
    let '(le, i2'') := leftMost i2' in
    match le, ri with
    | DIG 1, DIG 1 | DUG 1, DUG 1 => SEQ i1'' i2''
    | _, _ => SEQ i1' i2'
    end
  | DIP 1 i => DIP 1 (diug1diug1 i)
  | IF_ i1 i2 => IF_ (diug1diug1 i1) (diug1diug1 i2)
  | IF_NONE i1 i2 => IF_NONE (diug1diug1 i1) (diug1diug1 i2)
  | IF_LEFT i1 i2 => IF_LEFT (diug1diug1 i1) (diug1diug1 i2)
  | IF_CONS i1 i2 => IF_CONS (diug1diug1 i1) (diug1diug1 i2)
  | LOOP i => LOOP (diug1diug1 i)
  | LOOP_LEFT i => LOOP_LEFT (diug1diug1 i)
  | ITER i => ITER (diug1diug1 i)
  | MAP i => MAP (diug1diug1 i)
  | _ => ins
  end.

(** Eliminate PUSH; DROP sequences *)
Fixpoint push_drop (ins:untyped_syntax.instruction) : untyped_syntax.instruction :=
  match ins with
  | SEQ i1 i2 =>
    let i1' := (push_drop i1) in
    let i2' := (push_drop i2) in
    let '(i1'', ri) := rightMost i1' in
    let '(le, i2'') := leftMost i2' in
    match ri, le with
    | PUSH _ _, DROP 1 => SEQ i1'' i2''
    | _, _ => SEQ i1' i2'
    end
  | DIP 1 i => DIP 1 (push_drop i)
  | IF_ i1 i2 => IF_ (push_drop i1) (push_drop i2)
  | IF_NONE i1 i2 => IF_NONE (push_drop i1) (push_drop i2)
  | IF_LEFT i1 i2 => IF_LEFT (push_drop i1) (push_drop i2)
  | IF_CONS i1 i2 => IF_CONS (push_drop i1) (push_drop i2)
  | LOOP i => LOOP (push_drop i)
  | LOOP_LEFT i => LOOP_LEFT (push_drop i)
  | ITER i => ITER (push_drop i)
  | MAP i => MAP (push_drop i)
  | _ => ins
  end.

(** Recursively remove every noop instruction *)
Fixpoint clean_noop (ins:untyped_syntax.instruction) : untyped_syntax.instruction :=
  match ins with
  | SEQ i1 i2 =>
    match (clean_noop i1), (clean_noop i2) with
    | NOOP, NOOP => NOOP
    | NOOP, i'2 => i'2
    | i'1, NOOP => i'1
    | i'1, i'2 => SEQ i'1 i'2
    end
  | DIP 1 i =>
    match (clean_noop i) with
    | NOOP => NOOP
    | i' => DIP 1 i'
    end
  | IF_ i1 i2 => IF_ (clean_noop i1) (clean_noop i2)
  | IF_NONE i1 i2 => IF_NONE (clean_noop i1) (clean_noop i2)
  | IF_LEFT i1 i2 => IF_LEFT (clean_noop i1) (clean_noop i2)
  | IF_CONS i1 i2 => IF_CONS (clean_noop i1) (clean_noop i2)
  | LOOP i => LOOP (clean_noop i)
  | LOOP_LEFT i => LOOP_LEFT (clean_noop i)
  | ITER i => ITER (clean_noop i)
  | MAP i => MAP (clean_noop i)
  | _ => ins
  end.

(** Clean some stuff in the code *)
Definition cleanup (ins:untyped_syntax.instruction) : untyped_syntax.instruction :=
  clean_noop (push_drop
                (clean_noop (diug1diug1
                               (clean_noop (digndugn
                                              (clean_noop (dig0dug0 ins))))))).

(** Optimize the code (currently only cleanup of useless instructions *)
Definition optimize := cleanup.

(** Destructure a record into the stack before the function call *)
Fixpoint unpair_rec_to_stack (lt : list (label * ty)) : (untyped_syntax.instruction * list label) :=
  match lt with
  | nil => (DROP1, nil) (* Empty record = unit, just drop it *)
  | (l, _)::nil => (NOOP, l::nil)
  | (l, _)::tl => let (ins, ll) := unpair_rec_to_stack tl in (SEQ UNPAIR (DIP1 ins), l::ll)
  end.

(** Restructure the return record from the stack after the function call *)
Fixpoint pair_stack_to_rec (lt : list (label * ty)) (ll : list label) : M (untyped_syntax.instruction * list label) :=
  match lt with
  | nil => Return _ (PUSH syntax_type.unit Unit, ll)
  | (l, _)::nil => (compute_dig l ll)
  | (l, _)::tl => let%err (ins1, ll') := pair_stack_to_rec tl ll in
                let%err (dig, ll'') := compute_dig l ll' in
                Return _ (SEQ (SEQ ins1 dig) PAIR, tln ll'' 1)
  end.

(** Compile one of the toplevel functions *)
Definition compile_function (fv: fvar) (ins:typed_instr) (functs: list function) : M function :=
  let%err ((rty_record lt1), (rty_record lt2)) := types_of_typed_instr ins in
  let (ins1, ll1) := unpair_rec_to_stack lt1 in
  let%err (ins2, ll2) := compile_instr ins ll1 functs in
  let%err (ins3, _) := pair_stack_to_rec lt2 ll2 in
  let%err mty1 := michelson_type_of_type (ty_rty (rty_record lt1)) in
  let%err mty2 := michelson_type_of_type (ty_rty (rty_record lt2)) in
  Return _ (fv, mty1,
            (optimize (SEQ ins1 (SEQ ins2 ins3))),
            mty2).

Fixpoint compile_prog_aux (defs:list (fvar * typed_instr)) (functs: list function) : M (list function) :=
  match defs with
  | nil => Return _ (List.rev functs)
  | (fv, ins)::tl => let%err f := compile_function fv ins functs in
                   (compile_prog_aux tl (f::functs))
  end.

(** Compile all of the program *)
Definition type_and_compile_prog (defs: prog) :=
  let%err defs' := type_program defs in
  compile_prog_aux defs' nil.
