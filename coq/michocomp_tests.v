(**                        Albert to Mi-Cho-Coq compiler                       *)

Require Michocoq.untyped_syntax Michocoq.syntax Michocoq.syntax_type Michocoq.typer Michocoq.untyper.
Require aux albert error label_lexico lexico_sort typer Michocoq.syntax_type.

Import aux error.
Import albert typer  label_lexico lexico_sort.
Import Michocoq.untyped_syntax Michocoq.syntax_type.

Require Import List String Bool Ascii ZArith Permutation.
Require Import Coq.Program.Equality.

Require Import michocomp.

Module Tests.

Module ST.
  Definition self_type := syntax_type.unit.
End ST.

Module Context.
  Definition get_contract_type (_ : syntax.contract_constant) :=
    Michocoq.error.Failed syntax_type.type (Michocoq.error.Typing _ "contract typing not supported" ).
End Context.

(* Module comp := Michocomp ST Context. Import comp. *)

Open Scope string.

(* Albert value -> Michelson data *)
Definition michelson_of_record := Eval compute in (michelson_data_of_val (val_rval (rval_record (("b", (val_num (numval_int 1%Z)))::("a", val_num (numval_int 2%Z))::nil)))).
Goal michelson_of_record = Return concrete_data (Pair (Int_constant 2) (Int_constant 1)).
        reflexivity.
Qed.

Definition michelson_of_variant := Eval compute in (michelson_data_of_val (val_constr "B" (val_num (numval_int 42%Z)) (vty_variant (("A", ty_int)::("B", ty_int)::("C", ty_nat)::nil)))).
Goal michelson_of_variant = Return concrete_data (Right (Left (Int_constant 42))).
        reflexivity.
Qed.

(* Albert -> Michelson type *)
Definition michelson_of_nil_rty := Eval compute in (michelson_type_of_type (ty_rty (rty_record nil))).
Goal michelson_of_nil_rty = Return type unit.
        reflexivity.
Qed.

Definition michelson_of_rty := Eval compute in (michelson_type_of_type (ty_rty (rty_record (("c", ty_nat)::("b", ty_mutez)::("a", ty_int)::nil)))).
Goal michelson_of_rty = Return type (pair (Comparable_type nat) (pair (Comparable_type mutez) (Comparable_type int))).
        reflexivity.
Qed.

Definition michelson_of_vty_option := Eval compute in (michelson_type_of_type (ty_vty (vty_variant (("None", ty_unit)::("Some", ty_nat)::nil)))).
Goal michelson_of_vty_option = Return type (option (Comparable_type nat)).
        reflexivity.
Qed.

Definition michelson_of_vty := Eval compute in (michelson_type_of_type (ty_vty (vty_variant (("A", ty_unit)::("B", ty_nat)::nil)))).
Goal michelson_of_vty = Return type (or unit (Comparable_type nat)).
        reflexivity.
Qed.

(* Compiler tests *)

Definition startStack := ("b"::"m"::"or"::"v"::"x"::"y"::"z"::nil).
Definition startType := rty_record (("b", ty_bool)
                                      ::("m", ty_mutez)
                                      ::("or", ty_or ty_int ty_nat)
                                      ::("v", ty_vty (vty_variant (("A", ty_int)::("B", ty_int)::("C", ty_int)::nil)))
                                      ::("x", ty_int)
                                      ::("y", ty_nat)
                                      ::("z", ty_rty (rty_record (("a", ty_int)::("b", ty_mutez)::("c", ty_int)::nil)))::nil).

Import untyped_syntax.
Notation "UNPAIR%" := (SEQ DUP (SEQ CAR (DIP CDR))).

Definition type_and_compile ins rt ll := type_and_compile ins rt ll nil.

Definition assign_add := Eval compute in (type_and_compile (instr_assign (lhs_var "r") (rhs_add "x" "y")) startType startStack).
Goal assign_add = Return (instruction * Datatypes.list string) (SEQ (SEQ (SEQ (DIG 5) (DIG 5)) ADD) (DUG 3), "b" :: "m" :: "or" :: "r" :: "v" :: "z" :: nil).
  reflexivity.
Qed.

Definition assign_projection := Eval compute in (type_and_compile (instr_assign (lhs_var "p") (rhs_projection "z" "b")) startType startStack).
Goal assign_projection = Return (instruction * Datatypes.list string) (SEQ (SEQ (DIG 6) (SEQ CDR CAR)) (DUG 3), "b" :: "m" :: "or" :: "p" :: "v" :: "x" :: "y" :: nil).
  reflexivity.
Qed.

Definition assign_update := Eval compute in (type_and_compile (instr_assign (lhs_var "p") (rhs_update "z" (("b", "m")::("c","x")::nil))) startType startStack).
Goal assign_update =
Return (instruction * Datatypes.list string)
  (SEQ
     (SEQ (DIG 6)
        (SEQ (SEQ DUP (SEQ CAR (DIP 1 CDR)))
           (SEQ (DIP 1 (SEQ (SEQ DUP (SEQ CAR (DIP 1 CDR))) (SEQ (DROP 1) (SEQ (DIG 2) (SEQ (DIP 1 (SEQ (DROP 1) (DIG 3))) PAIR))))) PAIR)))
     (DUG 2), "b" :: "or" :: "p" :: "v" :: "y" :: nil).
  reflexivity.
Qed.

Definition assign_lhs_record := Eval compute in (type_and_compile (instr_assign (lhs_record (("b","d")::("a", "c")::nil)) (rhs_arg (arg_var "z"))) startType startStack).
Goal assign_lhs_record =
Return (instruction * Datatypes.list string)
  (SEQ (DIG 6) (SEQ (SEQ DUP (SEQ CAR (DIP 1 CDR))) (SEQ (DUG 2) (DUG 1))), "b" :: "c" :: "d" :: "m" :: "or" :: "v" :: "x" :: "y" :: nil).
  reflexivity.
Qed.

Definition assign_rhs_record:= Eval compute in (type_and_compile (instr_assign (lhs_var "r") (rhs_arg (arg_record (("a1", "x")::("a2","z")::nil)))) startType startStack).
Goal assign_rhs_record =
Return (instruction * Datatypes.list string) (SEQ (SEQ (SEQ (DIG 6) (DIG 5)) PAIR) (DUG 3), "b" :: "m" :: "or" :: "r" :: "v" :: "y" :: nil).
        reflexivity.
Qed.

Definition drop := Eval compute in (type_and_compile (instr_drop "y") startType startStack).
Goal drop =
Return (instruction * Datatypes.list string) (SEQ (DIG 5) (DROP 1), "b" :: "m" :: "or" :: "v" :: "x" :: "z" :: nil).
        reflexivity.
Qed.

Definition assign_dup:= Eval compute in (type_and_compile (instr_assign (lhs_var "v2") (rhs_app fun_dup (arg_var "y"))) startType startStack).
Goal assign_dup =
Return (instruction * Datatypes.list string) (SEQ (SEQ (DIG 5) (SEQ DUP PAIR)) (DUG 4), "b" :: "m" :: "or" :: "v" :: "v2" :: "x" :: "z" :: nil).
        reflexivity.
Qed.

Definition assign_fun_app := Eval compute in (type_and_compile (instr_assign (lhs_var "v2") (rhs_app (fun_constr "B" (vty_variant (("A", ty_int)::("B", ty_int)::("C", ty_int)::nil))) (arg_var "x"))) startType startStack).
Goal assign_fun_app =
Return (instruction * Datatypes.list string)
  (SEQ (SEQ (DIG 4) (SEQ (LEFT (Comparable_type int)) (RIGHT (Comparable_type int)))) (DUG 4), "b" :: "m" :: "or" :: "v" :: "v2" :: "y" :: "z" :: nil).
        reflexivity.
Qed.

Definition assign_option := Eval compute in (type_and_compile (instr_assign (lhs_var "opt") (rhs_app (fun_constr "Some" (vty_variant (("None", ty_unit)::("Some", ty_int)::nil))) (arg_var "x"))) startType startStack).
Goal assign_option =
Return (instruction * Datatypes.list string) (SEQ (SEQ (DIG 4) SOME) (DUG 2), "b" :: "m" :: "opt" :: "or" :: "v" :: "y" :: "z" :: nil).
        reflexivity.
Qed.

Definition assign_match_variant := Eval compute in (type_and_compile
                                 (instr_assign (lhs_var "res")
                                               (rhs_match "v" (branchesrhs_branches
                                                                 ((pattern_variant "A" "i", (rhs_arg (arg_var "i")))
                                                                    ::(pattern_variant "B" "i", (rhs_arg (arg_var "i")))
                                                                    ::(pattern_variant  "C" "i", (rhs_arg (arg_var "i")))::nil))))
                                 startType startStack).
Goal assign_match_variant =
Return (instruction * Datatypes.list string)
  (SEQ (SEQ (DIG 3) (IF_LEFT (DIG 0) (SEQ (DIG 3) (IF_LEFT (DIG 0) (DIG 0))))) (DUG 3), "b" :: "m" :: "or" :: "res" :: "x" :: "y" :: "z" :: nil).
        reflexivity.
Qed.

Definition match_variant := Eval compute in
      (type_and_compile
         (instr_match "v" ( branchesins_branches (
                                (pattern_variant "A" "i", (instr_seq (instr_drop "x") (instr_assign (lhs_var "r") (rhs_arg (arg_var "i")))))
                                  ::(pattern_variant "B" "i", (instr_seq (instr_drop "i") (instr_assign (lhs_var "r") (rhs_arg (arg_var "x")))))
                                  ::(pattern_variant "C" "i", (instr_seq (instr_drop "x")(instr_assign (lhs_var "r") (rhs_arg (arg_var "i")))))::nil)))
         startType startStack).
Goal match_variant =
Return (instruction * Datatypes.list string)
  (SEQ (DIG 3)
     (IF_LEFT (SEQ (DUG 1) (SEQ (SEQ (DIG 4) (DROP 1)) (SEQ (DIG 1) (DUG 3))))
        (SEQ (DIG 3)
           (IF_LEFT (SEQ (DUG 1) (SEQ (SEQ (DIG 1) (DROP 1)) (SEQ (DIG 3) (DUG 3)))) (SEQ (DUG 1) (SEQ (SEQ (DIG 4) (DROP 1)) (SEQ (DIG 1) (DUG 3))))))),
  "b" :: "m" :: "or" :: "r" :: "y" :: "z" :: nil).
        reflexivity.
Qed.

Definition loop_left := Eval compute in
      (type_and_compile
         (instr_loop_left "or" "o" (instr_assign (lhs_var "or")
                                                 (rhs_app (fun_constr "Left" (vty_variant (("Left", ty_int)::("Right", ty_nat)::nil)))
                                                          (arg_var "o"))))
         startType startStack).
Goal loop_left =
Return (instruction * Datatypes.list string)
  (SEQ (SEQ (DIG 2) (LOOP_LEFT (SEQ (DUG 2) (SEQ (SEQ (SEQ (DIG 2) (LEFT (Comparable_type nat))) (DUG 2)) (DIG 2))))) (DROP 1),
   "b" :: "m" :: "v" :: "x" :: "y" :: "z" :: nil).
  reflexivity.
Qed.

Definition loop_bool := Eval compute in
      (type_and_compile
         (instr_loop "b" (instr_assign (lhs_var "b")
                                       (rhs_app (fun_constr "True" (vty_variant (("False", ty_unit)::("True", ty_unit)::nil)))
                                                (arg_value (val_rval (rval_record nil))))))
         startType startStack).
Goal loop_bool =
Return (instruction * Datatypes.list string)
       (SEQ (DIG 0) (LOOP (SEQ (SEQ (SEQ (PUSH unit Unit) (SEQ (DROP 1) (PUSH (Comparable_type bool) True_))) (DUG 0)) (DIG 0))),
        "m" :: "or" :: "v" :: "x" :: "y" :: "z" :: nil).
  reflexivity.
Qed.

Definition type_and_fun_def := Eval compute in
      (type_and_compile_prog (
           (prog_defs (def_tdef "int" ty_int)
           (prog_defs (def_tdef "int+nat" (ty_vty (vty_variant (("A", (ty_var "int"))::("B", ty_nat)::nil))))
           (prog_defs (def_fdef "f0" (ty_rty (rty_record (("i", ty_int)::nil)))
                                (ty_rty (rty_record (("v", (ty_var "int+nat"))::nil)))
                                (instr_assign (lhs_var "v") (rhs_app (fun_constr "A" (vty_variant (("A", ty_int)::("B", ty_nat)::nil))) (arg_var "i"))))
           (prog_def (def_fdef "f1" (ty_rty (rty_record (("a", ty_int)::("ri", (ty_rty (rty_record (("i", ty_int)::nil))))::nil)))
                               (ty_rty (rty_record (("a", ty_int)::("rv", (ty_rty (rty_record (("v", (ty_var "int+nat"))::nil))))::nil)))
                               (instr_assign (lhs_var "rv") (rhs_app (fun_var "f0") (arg_var "ri"))))
        )))))).
Goal type_and_fun_def =
Return (Datatypes.list (string * type * instruction * type))
  (("f0", Comparable_type int, LEFT (Comparable_type nat), or (Comparable_type int) (Comparable_type nat))
   :: ("f1", pair (Comparable_type int) (Comparable_type int),
      SEQ (SEQ DUP (SEQ CAR (DIP 1 CDR))) (SEQ (SEQ (DIG 1) (LEFT (Comparable_type nat))) (SEQ (DIG 1) PAIR)),
      pair (Comparable_type int) (or (Comparable_type int) (Comparable_type nat))) :: nil).
        reflexivity.
Qed.

End Tests.
