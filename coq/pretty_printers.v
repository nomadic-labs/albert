Require Import aux albert error label_lexico lexico_sort.
Require Import List Ascii String.
Require Import ZArith DecimalString.

(** Displaying types *)
Open Scope string_scope.

Fixpoint string_of_type (t:ty) : string :=
  match t with
  | ty_rty rty =>  string_of_rty rty
  | ty_var v => v
  | ty_nat => "nat" | ty_int => "int" | ty_mutez => "mutez" | ty_timestamp => "timestamp"
  | ty_unit => "unit"
  | ty_prod ty1 ty2 => "("++(string_of_type ty1)++"*"++(string_of_type ty2)++")"
  | ty_vty (vty_variant vt) => "[ "
                               ++(String.concat ";" (List.map (fun '(l, t) => (l++":"++(string_of_type t))) vt))
                               ++" ]"
  | ty_option ty => "(option "++(string_of_type ty)++")"
  | ty_or ty1 ty2 => "("++(string_of_type ty1)++"+"++(string_of_type ty2)++")"
  | ty_bool => "bool"
  | ty_string => "string" | ty_bytes => "bytes"
  | ty_list ty => "(list "++(string_of_type ty)++")"
  | ty_map ty ty' => "(map "++(string_of_type ty)++" "++(string_of_type ty')++")"
  | ty_big_map ty ty' => "(bigmap "++(string_of_type ty)++" "++(string_of_type ty')++")"
  | ty_operation => "operation" | ty_key_hash => "key_hash" | ty_address => "address"
  | ty_contract ty => "(contract "++(string_of_type ty)++")"
  | ty_key => "key"
  | ty_signature => "signature"
  end
with string_of_rty (rt:rty) : string :=
       match rt with
         | (rty_record rt) =>
           "{ "
             ++(String.concat ";" (List.map (fun '(l, t) => (l++":"++(string_of_type t))) rt))
             ++" }"
       end.

Fixpoint string_of_pattern (pat:pattern) : string :=
  match pat with
  | pattern_any => "_"
  | pattern_variant constructor5 var => constructor5++" "++var
  | pattern_nil => "[ ]"
  | pattern_cons var_1 var_2 => var_1++"::"++var_2
  end.

Definition lf := (String "010" EmptyString).

Fixpoint string_of_lhs (l:lhs) : string :=
  match l with
  | lhs_var x => x
  | lhs_record r =>
    "{" ++ (String.concat "; " (List.map (fun '(x, y) => x ++ " = " ++ y) r))++ "}"
  end.

Fixpoint string_map (f : ascii -> string) (s : string) : string :=
  match s with
  | "" => ""
  | String c s => f c ++ string_map f s
  end.

Definition string_of_N n := DecimalString.NilZero.string_of_int (N.to_int n).
Definition string_of_Z z :=
  let s := DecimalString.NilZero.string_of_int (Z.to_int z) in
  match s with
  | String "-" _ => s
  | _ => String "+" s
  end.

Definition string_of_num_val (v:num_val) : string :=
  match v with
  | numval_nat n => string_of_N n
  | numval_int n => string_of_Z n
  | numval_timestamp (timestampconst_string s) => s
  | numval_timestamp (timestampconst_int n) => string_of_Z n
  | numval_mutez x =>
    DecimalString.NilZero.string_of_int (Z.to_int (tez.to_Z x)) ++ " utz"
  end.

Fixpoint string_of_value (v:value) : string :=
  match v with
  | val_rval (rval_record r) =>
    "{" ++
        String.concat "; "
        (List.map (fun '(l, v) => l ++ " = " ++ string_of_value v) r) ++
        "}"
  | val_num n => string_of_num_val n
  | val_constr c v vty =>
    "(" ++ c ++ " " ++ string_of_value v ++ " : " ++ string_of_type (ty_vty vty) ++ ")"
  | val_bool b => if b then "true" else "false"
  | val_string s =>
    """" ++
      string_map
      (fun c => match c with """"%char => String "\" """" | c => String c "" end) s ++
      """"
  | val_bytes s => "0x" ++ s
  | val_lval (lval_list l ty) =>
    "([" ++ String.concat "; " (List.map string_of_value l) ++ "] : list (" ++
         string_of_type ty ++ "))"
  | val_mval (mval_map l k v) =>
    "({|" ++ String.concat
          "; "
          (List.map
             (fun '(x, y) => string_of_value x ++ " |-> " ++ string_of_value y)
             l) ++ "|} : map (" ++
          string_of_type k ++ ") (" ++ string_of_type v ++ "))"
  | val_key x => x
  | val_signature x => x
  | val_add x =>
    match x with
    | addval_tz y => "tz" ++ y
    | addval_kt y => "KT1" ++ y
    end
  end.

Fixpoint string_of_arg (a:arg) : string :=
  match a with
  | arg_var x => x
  | arg_value v => string_of_value v
  | arg_record r =>
    "{" ++ String.concat "; " (List.map (fun '(x, y) => x ++ " = " ++ y) r) ++ "}"
  end.

Definition string_of_f (f:f) : string :=
  match f with
  | fun_var f => f
  | fun_dup => "dup"
  | fun_constr c vty => "(" ++ c ++ " : " ++ string_of_type (ty_vty vty) ++ ")"
  | fun_hash_key => "hash_key"
  | fun_blake2b => "blake2b"
  | fun_sha256 => "sha256"
  | fun_sha512 => "sha512"
  | fun_contract ty => "contract " ++ string_of_type ty
  | fun_address => "address"
  | fun_implicit_account => "implicit_account"
  end.

Fixpoint string_of_rhs (r:rhs) : string :=
  match r with
  | rhs_arg arg => string_of_arg arg
  | rhs_app f arg => string_of_f f ++ " " ++ string_of_arg arg
  | rhs_projection x l => x ++ "." ++ l
  | rhs_update r l =>
    "{" ++ r ++ " with " ++
        (String.concat "; " (List.map (fun '(l, v) => l ++ " = " ++ v) l))
        ++ "}"
  | rhs_add x y => x ++ " + " ++ y
  | rhs_sub x y => x ++ " - " ++ y
  | rhs_mul x y => x ++ " * " ++ y
  | rhs_ediv x y => x ++ " /mod " ++ y
  | rhs_lsl x y => x ++ " << " ++ y
  | rhs_lsr x y => x ++ " >> " ++ y
  | rhs_abs x => "abs " ++ x
  | rhs_is_nat x => "is_nat " ++ x
  | rhs_int x => "int " ++ x
  | rhs_match x (branchesrhs_branches b) =>
    "match " ++ x ++ "with" ++ lf ++
             (String.concat (lf ++ "| ")
                            (List.map (fun '(pat, r) =>
                                         string_of_pattern pat ++
                                                           " => " ++
                                                           string_of_rhs r) b)) ++
             lf ++ "end"
  | rhs_not x => "~ " ++ x
  | rhs_and x y => x ++ " && " ++ y
  | rhs_or x y => x ++ " || " ++ y
  | rhs_xor x y => x ++ " ^ " ++ y
  | rhs_concat x y => "concat " ++ x ++ " " ++ y
  | rhs_size x => "size " ++ x
  | rhs_slice x y z => "slice " ++ x ++ " " ++ y ++ " " ++ z
  | rhs_bytes_pack x => "pack " ++ x
  | rhs_bytes_unpack ty x => "unpack " ++ string_of_type ty ++ " " ++ x
  | rhs_eq x y => x ++ " == " ++ y
  | rhs_neq x y => x ++ " != " ++ y
  | rhs_inf x y => x ++ " < " ++ y
  | rhs_infeq x y => x ++ " <= " ++ y
  | rhs_sup x y => x ++ " > " ++ y
  | rhs_supeq x y => x ++ " >= " ++ y
  | rhs_list l => "[" ++ String.concat "; " l ++ "]"
  | rhs_list_cons x y => x ++ " :: " ++ y
  | rhs_list_map x y r => "map " ++ x ++ " in " ++ y ++ " do " ++ string_of_rhs r ++ " done"
  | rhs_map m =>
    "{|" ++ String.concat "; " (List.map (fun '(x, y) => x ++ " |-> " ++ y) m) ++ "|}"
  | rhs_map_map x y z r =>
    "map " ++ x ++ " |-> " ++ y ++ " in " ++ z ++ " do " ++ string_of_rhs r ++ " done"
  | rhs_map_get x y => x ++ "[" ++ y ++ "]"
  | rhs_map_update x y z => "{|" ++ x ++ " with " ++ y ++ " |-> " ++ z ++ "|}"
  | rhs_check_signature x y z => "check_signature " ++ x ++ " " ++ y ++ " " ++ z
  | rhs_amount => "amount"
  | rhs_self => "self"
  | rhs_transfer_tokens x y z => "transfer_tokens " ++ x ++ " " ++ y ++ " " ++ z
  | rhs_set_delegate x => "set_delegate " ++ x
  | rhs_sender => "sender"
  | rhs_source => "source"
  | rhs_now => "now"
end.

Fixpoint string_of_instruction (i:instruction) : string :=
  match i with
  | instr_noop => "noop"
  | instr_seq i1 i2 =>
    string_of_instruction i1 ++ ";" ++ lf ++ string_of_instruction i2
  | instr_assign l r =>
    string_of_lhs l ++ " = " ++ string_of_rhs r
  | instr_failwith arg =>
    "failwith " ++ string_of_arg arg
  | instr_drop l =>
    "drop " ++ l
  | instr_match l (branchesins_branches branches) =>
    "match " ++ l ++ " with" ++ lf ++
             (String.concat (lf ++ "| ")
                            (List.map
                               (fun '(pat, i) => string_of_pattern pat ++ " -> " ++ string_of_instruction i)
                               branches)) ++ lf ++
             "end"
  | instr_loop_left var var' i =>
    "loop_left " ++ var ++ " in " ++ var' ++ "do" ++ lf ++
                 string_of_instruction i ++ lf ++ "done"
  | instr_loop var i =>
    "loop " ++ var ++ " do" ++ lf ++ string_of_instruction i ++ lf ++ "done"
  | instr_list_for var var' i =>
    "for " ++ var ++ " in " ++ var' ++ " do" ++ lf ++ string_of_instruction i ++ lf ++ lf ++ "done"
  | instr_map_for var1 var2 var3 i =>
    "for " ++ var1 ++ " |-> " ++ var2 ++ " in " ++ var3 ++ " do" ++ lf ++
           string_of_instruction i ++ lf ++ "done"
  end.

Fixpoint string_of_def (d:definition) : string :=
  match d with
  | def_tdef tvar ty => "type " ++ tvar ++ " = " ++ string_of_type ty
  | def_fdef fvar ty ty' instruction =>
    "def " ++ fvar ++ " : " ++ string_of_type ty ++ " -> " ++ string_of_type ty'
           ++ " = " ++ lf ++ string_of_instruction instruction
  end.


Fixpoint string_of_prog (p:prog) : string :=
  match p with
  | prog_def d => string_of_def d
  | prog_defs d p => string_of_def d ++ lf ++ lf ++ string_of_prog p
  end.
