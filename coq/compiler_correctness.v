(**                        Albert to Mi-Cho-Coq compiler                       *)

Require Michocoq.untyped_syntax Michocoq.syntax Michocoq.syntax_type Michocoq.typer Michocoq.untyper.
Require aux albert error label_lexico lexico_sort typer Michocoq.syntax_type.

Import aux error.
Import albert typer  label_lexico lexico_sort.
Import Michocoq.untyped_syntax Michocoq.syntax_type.

Require Import List String Bool Ascii ZArith Permutation.
Require Import Coq.Program.Equality.
Require Import michocomp.

Module Proofs (C : syntax.ContractContext).
  Module syntax := syntax.Syntax C.
  Module mtyper := typer.Typer C.



  Parameter self_type : Datatypes.option type.
  Definition type_data := mtyper.type_data.
  Definition type_instruction := mtyper.type_instruction (self_type := self_type).
  Definition type_check_instruction := mtyper.type_check_instruction (self_type := self_type).

  Lemma type_check_instruction_type_instruction : forall ins A B ins',
      type_check_instruction type_instruction ins A B = Michocoq.error.Return ins' ->
      exists r, type_instruction ins A = Michocoq.error.Return r.
  Proof.
    intros ins A B ins' Hcheck.
    unfold type_check_instruction, mtyper.type_check_instruction in Hcheck.
    apply error.success_eq_return_rev.
    apply error.success_eq_return in Hcheck.
    apply error.success_bind_arg in Hcheck.
    assumption.
  Qed.

  Lemma data_type_correct: forall v d ty mty,
      michelson_data_of_val v = Return _ d ->
      type_value v = Return _ ty ->
      michelson_type_of_type ty = Return _ mty ->
      exists td, type_data d mty = Michocoq.error.Return td.
  Proof.
    (* induction v using val_ind; intros d ty' mty Hd Hty Hmty; simpl in *. *)
    (* - (* numval *) *)
    (*   admit. *)
    (*   (* AJ: this blocks coq. *)
    (*    * destruct v; invert Hd; invert Hty; invert Hmty; simpl; *)
    (*    *   unfold type_data, mtyper.type_data; eexists; eauto. *)
    (*    *) *)
    (* - (* bool *) *)
    (*   invert Hty; invert Hmty. *)
    (*   destruct b; simpl in *; invert Hd; eexists; reflexivity. *)
    (* - (* record *) *)
    (*   admit. *)
    (* - (* variant *) *)
    (*   admit. *)
  Admitted.

  Fixpoint michelson_stack_type_of_rty (lt : list (label * ty)) : M syntax.stack_type :=
    match lt with
    | nil => Return _ nil
    | (_, t)::tl => let%err sty := michelson_stack_type_of_rty tl in
                  let%err mty := michelson_type_of_type t in
                  Return _ (mty::sty)
    end.

  (* Lemma michelson_stack_type_of_rty_map_ret : forall lt, *)
  (*     michelson_stack_type_of_rty lt (map (fun '(l, _) => l) lt) = *)
  (*     Return _ (map (fun '(_, t) => (michelson_type_of_type t)) lt). *)
  (* Proof. *)
  (*   induction lt; simpl. *)
  (*   - reflexivity. *)
  (*   - destruct a as [hd t]. *)
  (*     destruct string_dec; try contradiction. *)
  (*     simpl. rewrite IHlt; simpl. *)
  (*     reflexivity. *)
  (* Qed. *)

  Lemma compute_dig_typing: forall var ty' lt lt' ll ll' ins mty sty sty',
      type_and_remove var lt = Return _ (ty', lt') ->
      compute_dig var ll = Return _ (ins, ll') ->
      michelson_type_of_type ty' = Return _ mty ->
      michelson_stack_type_of_rty lt = Return _ sty -> michelson_stack_type_of_rty lt' = Return _ sty' ->
      exists tins, type_instruction ins sty = Michocoq.error.Return (mtyper.Inferred_type sty (mty::sty') tins).
  Proof.
    (* TODO when the DIG typer is formally specified in michelson *)
    (* We could possibly give this tins explicitely instead of "exists" ? *)
  Admitted.

  Lemma stype_dec_same A : syntax_type.stype_dec A A = left eq_refl.
  Proof.
    destruct (syntax_type.stype_dec A A) as [e | n].
    - f_equal.
      apply Eqdep_dec.UIP_dec.
      apply syntax_type.stype_dec.
    - destruct (n eq_refl).
  Qed.

  Lemma string_dec_same s : string_dec s s = left eq_refl.
  Proof.
    destruct (string_dec s s) as [e | n].
    - f_equal.
      apply Eqdep_dec.UIP_dec. apply string_dec.
    - destruct (n eq_refl).
  Qed.

  Lemma unpair_pair: forall lt ins ll,
      unpair_rec_to_stack lt = (ins, ll) ->
      exists ins' ll', pair_stack_to_rec lt ll = Return _ (ins', ll').
  Proof.
    (* induction lt using list_hdhd_ins; intros ins ll Hunpair. *)
    (* - simpl in *. invert Hunpair. eexists; eauto. *)
    (* - simpl in *. destruct a as [l t]. *)
    (*   invert Hunpair. *)
    (*   repeat eexists. *)
    (*   unfold compute_dig. simpl. rewrite string_dec_same; simpl. *)
    (*   auto. *)
    (* - admit. *)
  Admitted.

  Ltac unfold_check :=
    unfold type_check_instruction, mtyper.type_check_instruction; simpl;
    try (unfold mtyper.type_check_instruction_no_tail_fail,
         mtyper.type_instruction_no_tail_fail); simpl.

  Ltac unfold_cast :=
    unfold mtyper.instruction_cast_range, mtyper.instruction_cast;
    simpl; repeat rewrite stype_dec_same; simpl.

  (** Arg adds a single element to the stack *)
  Lemma compile_arg_type_correct : forall arg ins ty' lt lt' ll ll' mty sty sty',
      type_arg arg (rty_record lt) = Return _ (ty', rty_record lt') ->
      compile_arg arg ll = Return _ (ins, ll') ->
      michelson_type_of_type ty' = Return _ mty ->
      michelson_stack_type_of_rty lt = Return _ sty -> michelson_stack_type_of_rty lt' = Return _ sty' ->
      exists res, type_check_instruction type_instruction ins sty (mty::sty') = Michocoq.error.Return res.
  Proof.
    (* induction arg; intros ins ty' lt lt' ll ll' mty sty sty' Hty Hcomp Hmty Hsty Hsty'; simpl in *. *)
    (* - (* var *) *)
    (*   apply bind_eq_return in Hty as [[ty'' lt''] [Hty Heq]]; invert Heq. *)
    (*   unfold_check. *)
    (*   apply bind_eq_return in Hcomp as [[insDig ll''] [Hcomp Hcomp']]; invert Hcomp'. *)
    (*   specialize (compute_dig_typing var _ _ _ _ _ _ _ _ _ Hty Hcomp Hmty Hsty Hsty') as [tins Hdig]. *)
    (*   rewrite Hdig; simpl. unfold_cast. *)
    (*   admit. *)
    (*   (* AJ: does not work anymore *)
    (*    * eexists reflexivity. *)
    (*    *) *)
    (* - (* val *) *)
    (*   apply bind_eq_return in Hty as [ty'' [Hty Hty2]]; invert Hty2. *)
    (*   apply bind_eq_return in Hcomp as [d [Hd1 Hd2]]. *)
    (*   apply bind_eq_return in Hd2 as [ty'' [Hd2 Hd3]]. *)
    (*   apply bind_eq_return in Hd3 as [td' [Hd3 Hd4]]; invert Hd4. *)
    (*   rewrite Hsty in Hsty'; invert Hsty'. rewrite Hty in Hd2; invert Hd2. *)
    (*   rewrite Hmty in Hd3; invert Hd3. *)
    (*   specialize (data_type_correct _ d ty'' td' Hd1 Hty Hmty) as [td Htd]. *)
    (*   unfold_check; unfold type_data in Htd. *)
    (*   (* rewrite Htd; simpl. unfold_cast. eexists; auto. *) *)
    (*   admit. *)
    (* - (* record *) admit. *)
  Admitted.

  Lemma compile_funct_type_correct : forall funct defs ins ty ty' mty mty' sty,
      type_funct funct ty g_empty = Return _ ty' ->
      compile_funct funct defs = Return _ ins ->
      michelson_type_of_type ty = Return _ mty -> michelson_type_of_type ty' = Return _ mty' ->
      exists res, type_check_instruction type_instruction ins (mty::sty) (mty'::sty) = Michocoq.error.Return res.
  Proof.
    (* intros funct defs ins ty ty' mty mty' sty Hty Hcomp Hmty Hmty'. *)
    (* induction funct; invert Hty; invert Hcomp; simpl in *. *)
    (* - (* dup *) *)
    (*   apply bind_eq_return in Hmty' as [mty'' [Hmty'' Hmty']]. *)
    (*   apply bind_eq_return in Hmty' as [mty''' [Hmty''' Hmty']]. *)
    (*   rewrite Hmty in Hmty''; rewrite Hmty in Hmty'''; invert Hmty''; invert Hmty'''. *)
    (*   invert Hmty'. *)
    (*   unfold_check. unfold_cast. *)
    (*   eexists. *)
    (*   admit. *)
    (*   (* AJ: no longer works *)
    (*    * reflexivity. *)
    (*    *) *)
    (* - (* constr *) *)
    (*   destruct vty5. *)
    (*   apply bind_eq_return in H0 as [[ty'' lt''] [Hret1 Hret2]]. *)
    (*   destruct (type_eq ty ty'') eqn:Heq; invert Hret2. *)
    (*   admit. *)
  Admitted.

  (** Rhs adds a single element to the stack *)
  Lemma compile_rhs_type_correct : forall rhs defs ins ty' lt lt' ll ll' mty sty sty',
      type_rhs rhs (rty_record lt) g_empty = Return _ (ty', rty_record lt') ->
      compile_rhs rhs ll lt defs = Return _ (ins, ll') ->
      michelson_type_of_type ty' = Return _ mty ->
      michelson_stack_type_of_rty lt = Return _ sty -> michelson_stack_type_of_rty lt' = Return _ sty' ->
      exists res, type_check_instruction type_instruction ins sty (mty::sty') = Michocoq.error.Return res.
  Proof.
    (* induction rhs; intros defs ins ty' lt lt' ll ll' mty sty sty' Hty Hcomp Hmty Hsty Hsty'; simpl in *. *)
    (* - (* arg *) *)
    (*   specialize (compile_arg_type_correct arg5 ins _ _ _ _ _ _ _ _ Hty Hcomp Hmty Hsty Hsty') as Harg. *)
    (*   assumption. *)
    (* - (* app *) *)
    (*   apply bind_eq_return in Hcomp as [[ins1 ll''] [Hcarg Hcomp]]. *)
    (*   apply bind_eq_return in Hcomp as [ins2 [Hcfunct Hret]]. invert Hret. *)
    (*   apply bind_eq_return in Hty as [[ty'' rt''] [Htarg Hty]]. *)
    (*   apply bind_eq_return in Hty as [ty''' [Htfunct Hret]]. invert Hret. *)
    (*   (* specialize (compile_arg_type_correct arg5 ins1 _ _ _ _ _ _ _ Htarg Hcarg Hsty Hsty') as [resa Harg]. *) *)
    (*   (* specialize (compile_funct_type_correct f5 ins2 _ _ sty' Htfunct Hcfunct) as [resf Hfunct]. *) *)
    (*   (* unfold_check.  *) admit. *)
    (* - (* projection *) admit. *)
    (* - (* update *) admit. *)
    (* - (* add *) *)
    (*   apply bind_eq_return in Hty as [[ty1 lt1] [Hty1 Hty]]; *)
    (*     apply bind_eq_return in Hty as [[ty2 lt2] [Hty2 Hty]]. *)
    (*   apply bind_eq_return in Hcomp as [[ins1 ll1] [Hcomp1 Hcomp]]; *)
    (*     apply bind_eq_return in Hcomp as [[ins2 ll2] [Hcomp2 Hcomp]]. invert Hcomp. *)
    (*   destruct ty1; destruct ty2; invert Hty; simpl; admit. *)
    (* - (* sub *) admit. *)
    (* - (* mul *) admit. *)
    (* - (* div *) admit. *)
    (* - (* match *) admit. *)
  Admitted.

  Lemma compile_instr_type_correct : forall ins defs lt lt' unins ll ll' sty sty',
      type_instr ins (rty_record lt) g_empty = Return _ (Inferred (rty_record lt')) ->
      type_and_compile ins (rty_record lt) ll defs = Return _ (unins, ll') ->
      michelson_stack_type_of_rty lt = Return _ sty ->
      michelson_stack_type_of_rty lt' = Return _ sty' ->
      exists res, type_check_instruction type_instruction unins sty sty' = Michocoq.error.Return res.
  Proof.
    (* induction ins; intros defs lt lt' unins ll ll' sty sty' Hty Hcomp Hsty Hsty'; *)
    (*   invert Hty; invert Hcomp; simpl in *. *)
    (* - (* noop *) *)
    (*   rewrite Hsty in Hsty'. invert Hsty'. *)
    (*   unfold_check. unfold_cast. *)
    (*   eexists. reflexivity. *)
    (* - (* seq *) *)
    (*    admit. *)
    (* - (* assign *) *)
    (*   admit. *)
    (* - (* drop *) *)
    (*   admit. *)
    (* - (* match *) *)
    (*   admit. *)
    (* - (* loop_left *) *)
    (*   admit. *)
    (* - (* loop *) *)
    (*   admit. *)
  Admitted.

End Proofs.
