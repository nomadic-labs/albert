(* Sorting modules, for both a (label * ty) list and a (label * label) list *)

Require Import label_lexico albert.
Require Import String Orders Sorting.Mergesort.

Module LexicoOrderTy <: TotalLeBool.
  Definition t := (string * ty)%type.
  Definition leb := @lexico_leb_pair ty.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2. destruct p1 as [s1 t1]; destruct p2 as [s2 t2].
    unfold leb. repeat rewrite lexico_leb_pair_le.
    apply lexico_total.
  Qed.
End LexicoOrderTy.
Module LexicoTySort := Sort LexicoOrderTy.

Module LexicoOrderVal <: TotalLeBool.
  Definition t := (string * value)%type.
  Definition leb := @lexico_leb_pair value.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2. destruct p1 as [s1 t1]; destruct p2 as [s2 t2].
    unfold leb. repeat rewrite lexico_leb_pair_le.
    apply lexico_total.
  Qed.
End LexicoOrderVal.
Module LexicoValSort := Sort LexicoOrderVal.

Module LexicoOrderLab <: TotalLeBool.
  Definition t := (string * label)%type.
  Definition leb := @lexico_leb_pair label.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2. destruct p1 as [s1 t1]; destruct p2 as [s2 t2].
    unfold leb. repeat rewrite lexico_leb_pair_le.
    apply lexico_total.
  Qed.
End LexicoOrderLab.
Module LexicoLabSort := Sort LexicoOrderLab.

Module LexicoOrderRhs <: TotalLeBool.
  Definition t := (string * label * rhs)%type.
  Definition leb := @lexico_leb_triple label rhs.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2. destruct p1 as [[s1 t1] t1']; destruct p2 as [[s2 t2] t2'].
    unfold leb. repeat rewrite lexico_triple_leb_le.
    apply lexico_total.
  Qed.
End LexicoOrderRhs.
Module LexicoRhsSort:= Sort LexicoOrderRhs.

Module LexicoPattern <: TotalLeBool.
  Definition t := pattern %type.

  Definition leb (p1 p2:pattern) :=
    match p1,p2 with
    | pattern_variant const1 var1,
      pattern_variant const2 var2 =>
      lexico_leb const1 const2
    | pattern_variant const1 var1, _  => true
    | pattern_nil , pattern_variant _ _   => false
    | pattern_nil ,  _   => true
    | pattern_cons var1 var2 , pattern_variant _ _   => false
    | pattern_cons var1 var2 , pattern_nil  => false
    | pattern_cons var1 var2 , _  => true
    | pattern_any, pattern_any => true
    | pattern_any, _ => false
    end.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2.
    destruct p1 ; destruct p2 ; unfold leb; auto.
    repeat rewrite lexico_leb_le.
    apply lexico_total.
  Qed.
End LexicoPattern.

Module LexicoPatternSort:= Sort LexicoPattern.


Module LexicoPattern_( T:Typ ) <: TotalLeBool.
  Definition t := (pattern*T.t) %type.

  Definition leb (p1 p2:(pattern*T.t)) :=
    let '(p1,_) := p1 in
    let '(p2,_) := p2 in
    LexicoPattern.leb p1 p2.

  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2.
    destruct p1 ; destruct p2 ; unfold leb; auto.
    apply LexicoPattern.leb_total.
  Qed.
End LexicoPattern_.


Module Ins <: Equalities.Typ .
  Definition t := instruction.
End Ins.

Module Rhs <: Equalities.Typ .
  Definition t := rhs.
End Rhs.

Module LexicoPatternIns_Sort.
  Module LexPat := (LexicoPattern_ Ins).
  Include Sort LexPat.
End LexicoPatternIns_Sort.


Module LexicoPatternRHS_Sort.
  Module LexPat := (LexicoPattern_ Rhs).
  Include Sort LexPat.
End LexicoPatternRHS_Sort.

Module LexicoOrderIns <: TotalLeBool.
  Definition t := (string * label * instruction)%type.
  Definition leb := @lexico_leb_triple label instruction.
  Theorem leb_total : forall s1 s2, leb s1 s2 = true \/ leb s2 s1 = true.
  Proof.
    intros p1 p2. destruct p1 as [[s1 t1] t1']; destruct p2 as [[s2 t2] t2'].
    unfold leb. repeat rewrite lexico_triple_leb_le.
    apply lexico_total.
  Qed.
End LexicoOrderIns.
Module LexicoInsSort:= Sort LexicoOrderIns.
