Require Import albert aux label_lexico typer subject_reduction.
Require Import List.

Hint Constructors eval_lhs.
Hint Constructors eval_arg.
Hint Constructors eval_rhs.
Hint Constructors eval_instr.

Theorem progress_lhs : forall lhs G E v ty ty',
    typing_lhs G lhs ty ty' ->
    typing_val G v ty ->
    exists v', eval_lhs E lhs v v'.
Proof.
  intros lhs G E v ty ty' Hlhs Hval.
  induction Hlhs.
  - (* var *)
    eexists; eauto.
  - (* record *)
    inversion Hval; subst; auto.
    rename l__x__ty__list into lxt; rename l__val__ty__list into lvt.
    assert (length lvt = length lxt) as Hlen by map_length H.
    symmetry in Hlen.
    remember (map (fun '((l,v,_),(_,x,_)) => (l,x,v)) (combine lvt lxt)) as lxv.
    assert (map (fun pat_ : label * label * value => let (p, _) := pat_ in let (l__, x__) := p in (l__, x__)) lxv =
            map (fun pat_ : label * label * ty => let (p, _) := pat_ in let (l__, x__) := p in (l__, x__)) lxt) as Hrew1.
    {
      subst. generalize Hlen H Hval H2; clear Hlen H Hval H2.
      generalize dependent lvt; generalize dependent lxt.
      refine (list_pair_ind _ _ _ _ _); intros; simpl in *.
      - reflexivity.
      - destruct a1 as [[a11 a12] a13]; destruct a2 as [[a21 a22] a23].
        invert H1. f_equal. apply H0; auto.
        apply typing_record_cons_inv in Hval; destruct Hval; auto.
    }
    rewrite <- Hrew1; clear Hrew1.
    assert (map (fun pat_ : label * label * value => let (p, val__) := pat_ in let (l__, _) := p in (l__, val__)) lxv =
            map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) lvt) as Hrew2.
    {
      subst. generalize Hlen H Hval H2; clear Hlen H Hval H2.
      generalize dependent lvt; generalize dependent lxt.
      refine (list_pair_ind _ _ _ _ _); intros; simpl in *.
      - reflexivity.
      - destruct a1 as [[a11 a12] a13]; destruct a2 as [[a21 a22] a23].
        invert H1. f_equal. apply H0; auto.
        apply typing_record_cons_inv in Hval; destruct Hval; auto.
    }
    rewrite <- Hrew2; clear Hrew2.
    eexists; econstructor.
Qed.

Theorem progress_arg : forall arg G E v ty ty',
    typing_arg G arg ty ty' ->
    typing_val G v ty ->
    exists v', eval_arg E arg v v'.
Proof.
  intros arg G E v ty ty' Harg Hval.
  induction Harg.
  - (* var *) invert Hval.
    assert (exists v', l__val__ty__list = (var, v', ty5)::nil) as Hv'.
    { destruct l__val__ty__list; invert H. destruct p as [[var' val'] ty'].
      invert H1. exists val'. f_equal. destruct l__val__ty__list; auto.
      invert H3. }
    destruct Hv' as [v' Hv']; subst; simpl in *.
    exists v'. constructor.
  - (* val *)
    inversion Hval; subst; auto.
    exists value5. apply map_eq_nil in H0. rewrite H0; simpl. auto.
  - (* record *)
    invert Hval. rename l__x__ty__list into lxt; rename l__val__ty__list into lvt.
    assert (length lxt = length lvt) as Hlen by map_length H.
    remember (map (fun '((_,v,_),(l,x,_)) => (l,x,v)) (combine lvt lxt)) as lxv.
    assert (map (fun pat_ : label * label * value => let (p, _) := pat_ in let (l__, x__) := p in (l__, x__)) lxv =
            map (fun pat_ : label * label * ty => let (p, _) := pat_ in let (l__, x__) := p in (l__, x__)) lxt) as Hrew1.
    {
      subst. generalize Hlen H H2; clear Hlen H H2.
      generalize dependent lvt; generalize dependent lxt.
      refine (list_pair_ind _ _ _ _ _); intros; simpl in *.
      - reflexivity.
      - destruct a1 as [[a11 a12] a13]; destruct a2 as [[a21 a22] a23].
        invert H1. f_equal. auto.
    }
    rewrite <- Hrew1; clear Hrew1.
    assert (map (fun pat_ : label * label * value => let (p, val__) := pat_ in let (_, x__) := p in (x__, val__)) lxv =
            map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) lvt) as Hrew2.
    {
      subst. generalize Hlen H H2; clear Hlen H H2.
      generalize dependent lvt; generalize dependent lxt.
      refine (list_pair_ind _ _ _ _ _); intros; simpl in *.
      - reflexivity.
      - destruct a1 as [[a11 a12] a13]; destruct a2 as [[a21 a22] a23].
        invert H1. f_equal. apply H0; auto.
    }
    rewrite <- Hrew2; clear Hrew2.
    eexists; econstructor.
Qed.

Theorem progress_f : forall f G E v ty ty',
    typing_f G f ty ty' ->
    typing_val G v ty ->
    exists v', eval_f E f v v'.
Proof.
  intros f G E v ty ty' Hf Hval.
  induction Hf.
  - admit.
Admitted.

Theorem progress_rhs : forall rhs G E v ty ty',
    typing_rhs G rhs ty ty' ->
    typing_val G v ty ->
    exists v', eval_rhs E rhs v v'.
Proof.
  intros rhs G E v ty ty' Hrhs Hval.
  induction Hrhs.
  - (* arg *)
    specialize (progress_arg arg5 g5 E v ty5 ty' H Hval) as [v' Harg].
    exists v'; auto.
  - (* f arg *)
    specialize (progress_arg arg5 g5 E v ty5 ty' H Hval) as [v' Harg].
    assert (typing_val g5 v' ty') as Hval' by (eapply sr_arg; eauto).
    specialize (progress_f f5 g5 E v' ty' ty'' H0 Hval') as [v'' Hf].
    exists v''; eauto.
  - (* projection *)
    invert Hval. rename l__val__ty__list into lvt.
    destruct rty5 as [ls1]; simpl in *.
    assert (In (l, ty5) (map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) lvt))
    as HIn by (eapply Join_In_l_cons; eauto).
    assert (exists val, In (l, val, ty5) lvt) as [val HIn'].
    { clear H0 H. induction lvt; simpl in *.
      + inversion HIn.
      + destruct a as [[l' val'] ty']. destruct HIn.
        * invert H. exists val'; left; auto.
        * apply IHlvt in H; auto. destruct H as [val H].
          exists val; right. exact H. }
    exists val.
    assert (In (l, val) (map (fun pat_ : label * albert.value * ty => let (p, _) := pat_ in let (l0, v) := p in (l0, v)) lvt)) as HIn''.
    { clear H0 H H4 HIn. induction lvt; simpl in *.
      + inversion HIn'.
      + destruct a as [[l' val'] ty']. destruct HIn'.
        * left; auto. inversion H; auto.
        * right; apply IHlvt; auto. }
    invert H0. rewrite map_map in *; simpl in *.
    specialize (@Join_In_inv albert.value (l, val)
                             (map (fun pat_ : label * albert.value * ty => let (p, _) := pat_ in let (l0, v) := p in (l0, v)) lvt)) as [lv HJoin]; auto.
    { clear H H3 H4 H6 HIn HIn' HIn''.
      induction lvt; simpl in *. constructor.
      invert H2. destruct a as [[l_ x_] ty_].
      constructor; auto. rewrite map_map in *. auto.
      rewrite <- map_tuple_4. auto. }
    apply eval_rhs_projection with (rval_record lv); assumption.
  - (* update *)
    destruct rty5 as [l]; destruct rty' as [l'].
    rewrite app_nil_r in *.
    invert Hval. rename l__var__ty__list into lvt; rename l__val__ty__list into lvt'.
    admit.
Admitted.

Theorem progress_ins : forall ins G E v ty ty',
    typing_instr G ins ty ty' ->
    typing_val G v ty ->
    exists v', eval_instr E ins v v'.
Proof.
  intros ins G E v ty ty' Hins Hval.
  generalize dependent v.
  induction Hins; intros v Hval; auto.
  - (* frame *)
    destruct rty_5 as [ls]; destruct rty' as [ls']; destruct rty'' as [ls''];
      destruct rty1 as [ls1]; destruct rty2 as [ls2].
    invert Hval; rename l__val__ty__list into lvt.
    admit.
  - (* noop *)
    inversion Hval; subst. apply typing_record_nil_inv_r in Hval.
    apply map_eq_nil in Hval; subst; simpl in *.
    exists (val_rval (rval_record nil)); constructor.
  - (* seq *)
    specialize (IHHins1 v Hval). destruct IHHins1 as [v' IHHins1].
    assert (typing_val g5 v' ty_2) as Hval' by (eapply sr_instr; eauto).
    specialize (IHHins2 v' Hval'). destruct IHHins2 as [v'' IHHins2].
    exists v''. econstructor; eauto.
  - (* assign *)
    specialize (progress_rhs rhs5 g5 E v a b H Hval) as [v' Hrhs].
    assert (typing_val g5 v' b) as Hval' by (eapply sr_rhs; eauto).
    specialize (progress_lhs lhs5 g5 E v' b c H0 Hval'); intros [v'' Hlhs].
    exists v''; eauto.
Admitted.
