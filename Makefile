OTT=ott

OTT_DIR=ott
TEX_DIR=doc
COQ_DIR=coq
EXTRACTION_DIR=extraction
PARSER_DIR=parser
export
OTT_OPTIONS_COQ=-merge true -coq_expand_list_types false -signal_parse_errors true
OTT_OPTIONS_LATEX=-signal_parse_errors true -tex_show_meta false -tex_wrap false

OTT_NO_COLOR=-colour false
OTT_TEXT_OPTIONS=-show_sort true -show_defns true

OTT_FILES_LIST = \
  base.ott \
  fail.ott \
  toplevel_definition.ott \
  arith.ott \
  unit.ott \
  pair.ott \
  drop.ott \
  dup.ott \
  pattern_matching.ott \
  variant.ott \
  option.ott \
  or.ott \
  bool.ott \
  string.ott \
  bytes.ott \
  compare.ott \
  list.ott \
  map.ott \
  crypto.ott \
  operation.ott # \
  # lambda.ott
  # set.ott

OTT_TEX = $(OTT_FILES_LIST:%.ott=$(TEX_DIR)/%.tex)
OTT_FILES = $(OTT_FILES_LIST:%=$(OTT_DIR)/%)
OTT_TEX_DOCUMENTED_LIST = \
  base.tex \
  unit.tex \
  pair.tex \
  drop.tex \
  dup.tex \
  variant.tex \
  or.tex \
  option.tex \
  bool.tex

OTT_TEX_DOCUMENTED =$(OTT_TEX_DOCUMENTED_LIST:%=$(TEX_DIR)/%)

COQ_FILES_LIST = \
	label_lexico.v error.v \
	int64.v tez.v \
	albert.v \
	aux.v lexico_sort.v \
	typer.v \
	michocomp.v \
	subject_reduction.v \
	progress.v
COQ_FILES=$(COQ_FILES_LIST:%=$(COQ_DIR)/%)

all: $(COQ_DIR)/Makefile $(COQ_FILES) all-vo compiler

test: compiler
	make -C examples test

COVERAGE_REPORT := _coverage_report
COVERAGE_OUTPUT := _coverage_output

EXPECTED_BISECT_FILE := ${CURDIR}/${COVERAGE_OUTPUT}/bisect

.PHONY: coverage-setup
coverage-setup:
	@mkdir -p ${COVERAGE_OUTPUT}
	@echo "Instrumenting extraction/dune. Should you wish to instrument any other dune files,"
	@echo "then use ./scripts/instrument_dune_bisect.sh before compiling to add"
	@echo "bisect_ppx preprocessing directive to those dune files"
	@echo
	@echo "Example:"
	@echo "  ./scripts/instrument_dune_bisect.sh package/dune"
	@echo
	@./scripts/instrument_dune_bisect.sh
ifneq (${EXPECTED_BISECT_FILE}, ${BISECT_FILE})
	@echo "Warning: BISECT_FILE isn't properly set. Run:"
	@echo "  export BISECT_FILE=${EXPECTED_BISECT_FILE}"
endif


.PHONY: coverage-clean
coverage-clean:
	@-rm -Rf ${COVERAGE_OUTPUT} ${COVERAGE_REPORT}

.PHONY: coverage-report-html
coverage-report-html:
	@bisect-ppx-report html -o ${COVERAGE_REPORT} ${COVERAGE_OUTPUT}/*.coverage
	@echo "Report should be available in ${COVERAGE_REPORT}/index.html"

.PHONY: coverage-report
coverage-report:
	@bisect-ppx-report summary --per-file ${COVERAGE_OUTPUT}/*.coverage


full: all doc test

doc : proposal_final.pdf

all-vo: $(COQ_DIR)/Makefile $(COQ_FILES)
	$(MAKE) -C coq
echo:
	@echo "OTT=$(OTT)"
	@echo "OTT_DIR=$(OTT_DIR)"
	@echo "TEX_DIR=$(TEX_DIR)"
	@echo "COQ_DIR=$(COQ_DIR)"
	@echo "OTT_OPTIONS_COQ=$(OTT_OPTIONS_COQ)"
	@echo "OTT_OPTIONS_LATEX=$(OTT_OPTIONS_LATEX)"
	@echo "OTT_NO_COLOR=$(OTT_NO_COLOR)"
	@echo "OTT_TEXT_OPTIONS=$(OTT_TEXT_OPTIONS)"
	@echo "OTT_FILES_LIST=$(OTT_FILES_LIST)"
	@echo "OTT_TEX=$(OTT_TEX)"
	@echo "OTT_FILES=$(OTT_FILES)"
	@echo "OTT_TEX_DOCUMENTED_LIST=$(OTT_TEX_DOCUMENTED_LIST)"
	@echo "OTT_TEX_DOCUMENTED=$(OTT_TEX_DOCUMENTED)"
	@echo "VOS_FILES=$(VOS_FILES)"
	@echo "VOS=$(VOS)"

ott_tex: $(OTT_TEX)

$(COQ_DIR)/Makefile: $(COQ_DIR)/_CoqProject
	cd $(COQ_DIR); coq_makefile -f _CoqProject -o Makefile

$(COQ_DIR)/%.vo: $(COQ_DIR)/Makefile
	$(MAKE) -C $(COQ_DIR) $*.vo

$(COQ_DIR)/albert.v : $(OTT_DIR)/header.ott $(OTT_FILES)
	$(OTT) $(OTT_OPTIONS_COQ) -o $@ $^

$(TEX_DIR)/albert.tex : $(OTT_FILES) $(OTT_DIR)/latex.ott
	$(OTT) -merge true $(OTT_OPTIONS_LATEX) -tex_name_prefix albert -o $@ $^

$(TEX_DIR)/proposal_final.tex : $(OTT_FILES) $(OTT_TEX_DOCUMENTED) $(TEX_DIR)/proposal.tex
	$(OTT) -merge true $(OTT_OPTIONS_LATEX) -tex_name_prefix albert -tex_filter $(TEX_DIR)/proposal.tex $(TEX_DIR)/proposal_final.tex $^

OTT_TEX_BASE=$(OTT_DIR)/latex.ott $(OTT_DIR)/base.ott
$(TEX_DIR)/base.tex:     $(OTT_TEX_BASE)
$(TEX_DIR)/pattern_matching.tex:  $(OTT_TEX_BASE) $(OTT_DIR)/pattern_matching.ott
$(TEX_DIR)/unit.tex:     $(OTT_TEX_BASE) $(OTT_DIR)/unit.ott
$(TEX_DIR)/drop.tex:     $(OTT_TEX_BASE) $(OTT_DIR)/unit.ott $(OTT_DIR)/drop.ott
$(TEX_DIR)/pair.tex:     $(OTT_TEX_BASE) $(OTT_DIR)/pair.ott
$(TEX_DIR)/dup.tex:      $(OTT_TEX_BASE) $(OTT_DIR)/pair.ott $(OTT_DIR)/dup.ott
$(TEX_DIR)/variant.tex:  $(OTT_TEX_BASE) $(OTT_DIR)/pattern_matching.ott $(OTT_DIR)/variant.ott
$(TEX_DIR)/or.tex:       $(OTT_TEX_BASE)  $(OTT_DIR)/pattern_matching.ott $(OTT_DIR)/variant.ott  $(OTT_DIR)/or.ott
$(TEX_DIR)/option.tex:   $(OTT_TEX_BASE) $(OTT_DIR)/unit.ott $(OTT_DIR)/pattern_matching.ott $(OTT_DIR)/variant.ott $(OTT_DIR)/option.ott
$(TEX_DIR)/bool.tex:     $(OTT_TEX_BASE) $(OTT_DIR)/unit.ott $(OTT_DIR)/pattern_matching.ott $(OTT_DIR)/variant.ott $(OTT_DIR)/bool.ott
$(TEX_DIR)/arith.tex:    $(OTT_TEX_BASE) $(OTT_DIR)/unit.ott $(OTT_DIR)/pattern_matching.ott $(OTT_DIR)/variant.ott $(OTT_DIR)/option.ott $(OTT_DIR)/arith.ott $(OTT_DIR)/bool.ott $(OTT_DIR)/string.ott $(OTT_DIR)/bytes.ott $(OTT_DIR)/operation.ott $(OTT_DIR)/compare.ott
$(TEX_DIR)/%.tex:
	$(OTT) -merge true $(OTT_OPTIONS_LATEX) -tex_name_prefix $* -o $@ $^

$(TEX_DIR)/albert.txt : $(OTT_FILES)
	$(OTT) $(OTT_OPTIONS_COQ) $(OTT_TEXT_OPTIONS) $^
	$(OTT) $(OTT_OPTIONS_COQ) $(OTT_TEXT_OPTIONS) $(OTT_NO_COLOR) $^ > $@


$(TEX_DIR)/%.pdf:
	make -C $(TEX_DIR) $*.pdf

$(TEX_DIR)/proposal.tex: $(TEX_DIR)/proposal.org
	emacs --batch --file $< -f org-latex-export-to-latex --kill

proposal_final.pdf: $(TEX_DIR)/proposal_final.tex $(TEX_DIR)/latex_header.tex $(OTT_TEX_DOCUMENTED) $(TEX_DIR)/albert.tex $(TEX_DIR)/bibtex.bib
	$(MAKE) -C $(TEX_DIR) proposal_final.pdf && \
	cp $(TEX_DIR)/proposal_final.pdf ./


PARSER_ML=extraction/parser.ml
PARSER_MLI=extraction/parser.mli
PARSER_MLS=$(PARSER_ML) $(PARSER_MLI)
LEXER_ML=extraction/lexer.ml

compiler: all-vo $(PARSER_MLS) $(LEXER_ML)  extraction/Makefile
	$(MAKE) -C extraction

# Parser and pretty printer
PARSER_FILE=parser.mly
PARSER_PP_FILE=parser_pp.ml
LEXER_FILE=lexer.mll
AST_FILE=ast.ml

PARSER=$(PARSER_DIR)/parser.mly
PARSER_PP=$(PARSER_DIR)/parser_pp.ml
LEXER=$(PARSER_DIR)/lexer.mll
AST=$(PARSER_DIR)/ast.ml


$(PARSER) $(LEXER): $(OTT_FILES)
	cd parser;	$(OTT) -merge true $(^:%.ott=-i ../%.ott) -o $(AST_FILE) -o $(PARSER_FILE) -o $(LEXER_FILE) \
	-tex_suppress_ntr g
# Rewrite the parser to fit the extracted code
	sed -i -e 's/(\*Case .*) \(['A'-'Z']\)/Coq_\L\1/' $(PARSER)
	sed -i -e 's/Ast/Albert/' $(PARSER)
	sed -i -e 's/(constructor_0,var_0,ins_0)/((constructor_0,var_0),ins_0)/' $(PARSER)
	sed -i -e 's/(constructor_0,var_0,rhs_0)/((constructor_0,var_0),rhs_0)/' $(PARSER)
# And correct some stuff in the lexer
	sed -i -e 's/(num_litteral)/(Aux.uint_of_string num_litteral)/' $(LEXER)
	sed -i -e 's/(int_litteral)/(Aux.int_of_string int_litteral)/' $(LEXER)

$(PARSER_MLS): $(PARSER)
	menhir --explain --base $(EXTRACTION_DIR)/parser $(PARSER_DIR)/menhir_lib.mly $^

extraction/lexer.ml: $(LEXER)
	ocamllex $^ -o $@

clean:: coverage-clean
	rm -f $(OTT_TEX) proposal_final.pdf
	cd $(TEX_DIR); rm -f  *.aux *.bbl *.blg *.log *.out *.pdf *.toc
	cd $(COQ_DIR); rm -f *.vo *.glob albert.* Makefile Makefile.conf
	rm -f $(PARSER) $(LEXER) $(PARSER_PP) $(AST)
	make -C extraction clean 
	make -C examples clean 
	find -name "dune-project" -delete
build-deps:
	opam install --deps-only ./

install: all
	dune build @install
	dune install

uninstall:
	dune build @install
	dune uninstall
