#! /bin/bash

ret=0

for d in *
do
    while read -r f
    do
        bash lintfile.sh "$f" || ret=1
    done < <(find "$d" -type f)
done

exit $ret
