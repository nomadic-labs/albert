#!/bin/sh

set -e

usage () {
    cat >&2 <<EOF
usage: $0

Must be run from the root of Albert. Instruments to compiler for
coverage measuring, runs the full test suite, and generates the HTML
and ascii reports.

EOF
}

if [ "$1" = "--help" ]; then
    usage
    exit 1
fi

if [ ! -f coq-albert.opam ]; then
   usage;
   exit 1
fi

export BISECT_FILE=$(pwd)/_coverage_output/bisect
make coverage-clean
make coverage-setup
make test
make coverage-report
make coverage-report-html
