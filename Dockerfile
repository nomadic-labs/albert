FROM coqorg/coq as builder

RUN opam update -y
RUN sudo apt-get update -y -q
RUN DEBIAN_FRONTEND=noninteractive sudo apt-get install -y -q libgmp-dev findutils m4 perl ruby
RUN opam switch ${COMPILER_EDGE}
RUN opam repo add coq-released https://coq.inria.fr/opam/released
RUN opam repo add coq-extra-dev https://coq.inria.fr/opam/extra-dev
RUN opam install -y ott zarith
COPY . .
RUN opam pin add -y --no-action coq-albert .
RUN opam depext -y coq-albert
RUN opam install -y -j ${NJOBS} --deps-only .
RUN opam install -y -j ${NJOBS} --with-test .
RUN mkdir /home/coq/project
WORKDIR /home/coq/project
RUN sudo cp /home/coq/.opam/${COMPILER_EDGE}/bin/albert_c .

FROM debian:buster-slim
COPY --from=builder /home/coq/project/albert_c /usr/local/bin/albert_c
ENTRYPOINT ["/usr/local/bin/albert_c"]
