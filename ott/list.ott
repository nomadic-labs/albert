grammar

ty, a, b, c :: 'ty_' ::=
  | list a :: :: list

pattern :: 'pattern_' ::=
  | [ ] :: :: nil
  | var_1 '::' var_2 :: :: cons

 rhs :: 'rhs_' ::=
  | [ var_1 ; ... ; var_n ] :: :: list
  | var_1 '::' var_2 :: :: list_cons
  | map var in var' do rhs done :: :: list_map

instruction, I :: 'instr_' ::=
  | for var in var' do instruction_1 done :: :: list_for

lval, tl :: 'lval_' ::=
  | ( [ value_1 ; .. ; value_n ] : list ty ) :: :: list
  | value '::' tl :: M :: cons {{coq (let 'lval_list tls ty := [[tl]] in lval_list (List.cons [[value]] tls) ty)}} {{ocaml ([[value]]::[[tl]])}}

num_val, nv :: 'natval_' ::= {{coq Z}}
  | size lval :: M :: list_size
  {{coq (numval_nat (N.of_nat (let 'lval_list l ty := [[lval]] in List.length l)))}} {{tex |[[lval]]|}} {{ocaml (List.length [[lval]])}}

value, val :: 'val_' ::=
  | lval :: :: lval

defns
  Jtype :: 'typing_' ::=

  defn
    g |- ty :: :: ty_well_formed :: twf_ by


    g |- ty
    ------------ :: list
    g |- list ty

  defn
    tvar not free in ty :: :: fresh_tvar :: Tfresh_tvar {{com Type variable freshness}} by

    tvar not free in ty
    ------------------------ :: list
    tvar not free in list ty

  defn
    g |- val : ty :: :: val :: Tval_ by

    g |- val_1 : ty .. g |- val_k : ty
    -------------------------------------------------- :: list
    g |- ([val_1; .. ; val_k] : list ty) : list ty

  defn
    g |- instruction : ty => ty' :: :: instr :: T_ {{ com Typing }} by

    { var' : list ty } @ rty = rty'
    { var : ty } @ rty = rty''
    g |- instruction_1 : rty'' => rty
    -------------------------------------------------------------------------- :: list_for
    g |- for var in var' do instruction_1 done : rty' => rty

    { var : list ty } @ rty = rty'
    { var_1 : ty ; var_2 : list ty } @ rty = rty''
    g |- instruction_1 : rty => ty'
    g |- instruction_2 : rty'' => ty'
    -------------------------------------------------------------------------------------------------------------- :: list_match
    g |- match var with [ ] -> instruction_1 | var_1 :: var_2 -> I_2 end : rty' => ty'

  defn
    g |- rhs : ty => ty' :: :: rhs :: Trhs_ by

    ------------------------------------------------------------------------- :: list
    g |- [ var_1 ; ... ; var_n ] : { var_1 : ty ; ... ; var_n : ty } => list ty

    ---------------------------------------------------------- :: list_cons
    g |- x_1 :: x_2 : { x_1 : ty ; x_2 : list ty } => list ty

    ------------------------------------ :: list_size
    g |- size x : { x : list ty } => nat

    { var : list ty } @ rty = rty'
    { var_1 : ty ; var_2 : list ty } @ rty = rty''
    g |- rhs_1 : rty => ty'
    g |- rhs_2 : rty'' => ty'
    ---------------------------------------------------------------------------------------------- :: list_match
    g |- match var with [ ] -> rhs_1 | var_1 :: var_2 -> rhs_2 end : rty' => ty'


    g |- rhs : { var : ty } => ty'
    ----------------------------------------------------------------- :: list_map
    g |- map var in var' do rhs done : { var' : list ty } => list ty'

defns
  Jeval :: 'eval_' ::=

  defn
    E |- rhs / val -> val' :: :: rhs :: rhs_ {{com Right-hand side evaluation}} by

    ----------------------------------------------------------------------------------------------------------- :: list
    E |- [ var_1 ; .. ; var_n ] / { var_1 = val_1 ; .. ; var_n = val_n } -> ([ val_1 ; .. ; val_n ] : list ty)

    -------------------------------------------------------------------- :: list_cons
    E |- x_1 :: x_2 / { x_1 = val_1 ; x_2 = lval_2 } -> val_1 :: lval_2

    ------------------------------------------ :: list_size
    E |- size x / { x = lval } -> size lval

    { var = ([] : list ty) } @ rval = rval'
    E |- rhs_1 / rval -> val
    ------------------------------------------------------------------------------------------- :: list_match_nil
    E |- match var with [] -> rhs_1 | var_1 :: var_2 -> rhs_2 end / rval' -> val

    { var = val_1 :: tl } @ rval = rval'
    { var_1 = val1 ; var_2 = tl } @ rval = rval''
    E |- rhs_2 / rval'' -> val
    --------------------------------------------------------------------------------------------------- :: list_match_cons
    E |- match var with [] -> rhs_1 | var_1 :: var_2 -> rhs_2 end / rval' -> val

    ------------------------------------------------------------------------------------------- :: list_map_nil
    E |- map var in var' do rhs done / { var' = ([] : list ty) } -> ([] : list ty)

    E |- rhs / { var = val } -> val'
    E |- map var in var' do rhs done / { var' = tl } -> tl'
    ------------------------------------------------------------------------- :: list_map_cons
    E |- map var in var' do rhs done / { var' = val :: tl } -> val' :: tl'

  defn
    E |- instruction / val -> val' :: :: instr :: instr_ {{com Instruction evaluation}} by

    { var = ([] : list ty) } @ rval = rval'
    E |- instruction_1 / rval -> val
    ------------------------------------------------------------------------------------------- :: list_match_nil
    E |- match var with [] -> instruction_1 | var_1 :: var_2 -> I_2 end / rval' -> val

    { var_1 = val_1 :: tl } @ rval = rval'
    { var_1 = val_1 ; var_2 = tl } @ rval = rval''
    E |- instruction_2 / rval'' -> val
    ----------------------------------------------------------------------------------------------- :: list_match_cons
    E |- match var with [] -> instruction_1 | var_1 :: var_2 -> instruction_2 end / rval' -> val

    { var' = ([] : list ty) } @ rval = rval'
    -------------------------------------------------------------------------------------------- :: list_for_nil
    E |- for var in var' do instruction_1 done / rval' -> rval

    { var = val } @ rval = rval'
    E |- instruction_1 / rval' -> rval1
    { var' = tl } @ rval1 = rval''
    E |- for var in var' do instruction_1 done / rval'' -> rval2
    -------------------------------------------------------------------------------------------- :: list_for_cons
    E |- for var in var' do instruction_1 done / { var' = val :: tl } -> rval2
