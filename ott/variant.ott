
grammar

constructor :: 'user_constr_' ::= {{coq string}} {{ocaml (list char)}}
  | id :: S :: id {{coq [[id]]}} {{ocaml (Aux.explode [[id]])}}

label, l, var, x :: 'label_' ::= {{ coq-equality }} {{com Label / variable}}

ty, a, b, c :: 'ty_' ::=
  | vty :: :: vty

vty :: 'vty_' ::=
  | [ constructor_1 : ty_1 '|' ... '|' constructor_k : ty_k ] :: :: variant

f :: 'fun_' ::=
  | ( constructor : vty ) :: :: constr

pattern :: 'pattern_' ::=
  | constructor var :: :: variant

value, val :: 'val_' ::=
  | ( constructor value : vty ) :: :: constr

formula :: 'formula_' ::=
  | permutation vty vty' :: :: vty_permutation {{coq (let 'vty_variant l1 := [[vty]] in let 'vty_variant l2 := [[vty']] in Permutation l1 l2)}} {{tex \text{$[[vty']]$ is a permutation of $[[vty]]$} }}
  | distinct { constructor_1 ; .. ; constructor_k } :: :: distinct_ctor {{coq all_distinct string_dec [[constructor_1..constructor_k]] = true}} {{tex \text{constructors $constructor_1$ .. $constructor_k$ are distinct} }}
  | sorted { constructor_1 ; .. ; constructor_k } :: :: sorted_constr
  {{coq (Sorted.StronglySorted lexico_lt [[constructor_1..constructor_k]])}}
  | constructor_1 <> constructor_2 :: :: neq_ctor {{coq ([[constructor_1]] <> [[constructor_2]])}}
  | vty @ vty' = vty'' :: :: joinVariant
  {{coq (let 'vty_variant l := [[vty]] in let 'vty_variant l' := [[vty']] in let 'vty_variant l'' := [[vty'']] in Join l l' l'')}}

defns
  Jtype :: 'typing_' ::=

  defn
    g |- ty_1 == ty_2 :: :: type_eq :: Teq_ {{com Type equality}} by

    g |- ty_1 == ty'_1 .. g |- ty_k == ty'_k
    ------------------------------------------------------------------------------------------------------------------- :: variant_congr
    g |- [ constructor_1 : ty_1 | .. | constructor_k : ty_k ] == [ constructor_1 : ty'_1 | .. | constructor_k : ty'_k ]

  defn
    g |- ty :: :: ty_well_formed :: twf_ {{com Type well-formedness}} by

    sorted { constructor_1 ; .. ; constructor_k }
    g |- ty_1 .. g |- ty_k
    g |-
    --------------------------------------------------------- :: variant
    g |- [ constructor_1 : ty_1 | .. | constructor_k : ty_k ]

  defn
    tvar not free in ty :: :: fresh_tvar :: Tfresh_tvar {{com Type variable freshness}} by

    tvar not free in ty_1 .. tvar not free in ty_k
    --------------------------------------------------------------------- :: variant
    tvar not free in [ constructor_1 : ty_1 | .. | constructor_k : ty_k ]

  defn
    g |- rhs : ty => ty' :: :: rhs :: Trhs_ by

    {var : [ constructor_1 : ty_1 | .. | constructor_k : ty_k ]} @ rty = rty'
    {var_1 : ty_1} @ rty = rty_1 .. {var_k : ty_k} @ rty = rty_k
    g |- rhs_1 : rty_1 => ty .. g |- rhs_k : rty_k => ty
    ------------------------------------------------------------------------------------------------------------- :: match
    g |- match var with constructor_1 var_1 -> rhs_1 | .. | constructor_k var_k -> rhs_k end  : rty' => ty

  defn
    g |- instruction : ty => ty' :: :: instr :: T_  by

    { var :  [ constructor_1 : ty_1 | .. | constructor_k : ty_k ]} @ rty = rty'
    {var_1 : ty_1} @ rty = rty_1 .. {var_k : ty_k} @ rty = rty_k
    g |- I_1 : rty_1 => ty .. g |- I_k : rty_k => ty
    ------------------------------------------------------------------------------------------------------------- :: match
    g |- match var with constructor_1 var_1 -> I_1 | .. | constructor_k var_k -> I_k end  : rty' => ty


  defn
    g |- value : ty :: :: val :: Tval_ by

    g |- value : ty'
    [ constructor : ty' ] @ vty' = vty
    --------------------------------------------------------------------------------------------- :: constructor
    g |- (constructor value : vty) : vty

  defn
    g |- f : ty => ty' :: :: f :: Tf_ by

    [ constructor : ty' ] @ vty' = vty
    --------------------------------------------------------------------------------------------------------- :: constructor
    g |- (constructor : vty) : ty' => vty

defns
  Jeval :: 'eval_' ::=

  defn
    f : val -> val' :: :: f :: f_ {{com Function symbol evaluation}} by

    ---------------------------------------------------- :: constructor
    E |- (constructor : vty) / val -> (constructor val : vty)

  defn
    rhs : val -> val' :: :: rhs :: rhs_ {{com Right-hand side evaluation}} by

    E |- rhs / { var' = val' } -> val
    ------------------------------------------------------------------------------------------------------- :: match_found
    E |- match var with constructor var' -> rhs | <branches_rhs> end / { var = (constructor val' : vty) } -> val

    constructor <> constructor'
    E |- match var with branches_rhs end / { var = (constructor val' : vty) } -> val'
    ------------------------------------------------------------------------------------------------------- :: match_next
    E |- match var with constructor' var' -> rhs | <branches_rhs> end / { var = (constructor val' : vty) } -> val

  defn
    instruction : val -> val' :: :: instr :: instr_ {{com Instruction evaluation}} by

    { var = (constructor val' : vty) } @ rval = rval'
    { var' = val' } @ rval = rval'
    E |- I / rval' -> rval1
    ------------------------------------------------------------------------------------------------------- :: match_found
    E |- match var with constructor var' -> I | <branches_ins> end / rval' -> rval1

    { var = (constructor val' : vty) } @ rval = rval'
    constructor <> constructor'
    E |- match var with branches_ins end / rval' -> rval1
    -------------------------------------------------------------------------------------------------------- :: match_next
    E |- match var with constructor' var' -> I | <branches_ins> end / rval' -> rval1
