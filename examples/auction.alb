type parameter_ty = key_hash
type storage_ty = { endDate : timestamp ; highestBid : mutez ; lastBidder : key_hash }

def bid : { params : parameter_ty ; storage : storage_ty } -> { op : list operation ; storage : storage_ty } =
    // Check if the auction has ended
    (sto0, storage) = dup storage;
    endDate = sto0.endDate;
    no = now; ok = no < endDate;
    match ok with
    True u -> drop u
    | False u -> failwith "The auction is over"
    end;

    // Check if the new bid is higher than best
    (sto0, storage) = dup storage;
    highest = sto0.highestBid;
    am = amount; ok = am > highest;
    match ok with
    True u -> drop u
    | False u -> failwith "Not enough tez sent to replace highest bidder"
    end;

    // Refund the previous bidder
    (sto0, storage) = dup storage;
    { endDate = endDate ; highestBid = highest ; lastBidder = last } = sto0;
    drop endDate;
    un = {};
    acc = implicit_account last;
    transfer = transfer_tokens un highest acc;
    op = ([] : list operation);
    op = transfer :: op;

    // Save the new storage
    am = amount;
    storage = { storage with lastBidder = params ; highestBid = am }
