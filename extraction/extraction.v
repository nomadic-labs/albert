(* Extraction of the compiler *)

Require Michocoq.michelson2micheline Michocoq.micheline_pp.

Require Extraction.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.

Require Import Albert.michocomp.
Extraction Blacklist Char.
Recursive Extraction Library michocomp.
Recursive Extraction Library michelson2micheline.
Recursive Extraction Library micheline_pp.
