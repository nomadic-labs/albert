#! /bin/bash

f=$1
tail -c 1 "$f" | od -ta | grep -q nl
if [ $? -eq 1 ]
then
    echo "file $f is not terminated by a newline character" >&2
    echo >> "$f"
    exit 1
fi
